import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;

public class Teste {
	public static void main(String[] args) {
		ArrayList<String> teste = new ArrayList<String>();
		teste.add("INGRID");
		teste.add("INGRID_CONFIG");
		teste.add("INGRID_DATA");
		teste.add("INGRID_DATA_INT_E3");

		teste = orderDataSourceNames(teste);
		for (String string : teste) {
			System.out.println(string);
		}
		String usuario = System.getProperty("user.name");
		String machine = "";
		String machine_2 = "";
		try {
			machine = InetAddress.getLocalHost().getHostName();
			machine_2 = InetAddress.getLocalHost().getCanonicalHostName();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}

		System.out.println("Usuario " + usuario);
		System.out.println("Maquina " + machine);
		System.out.println("Maquina " + machine_2);
	}

	private static ArrayList<String> orderDataSourceNames(
			ArrayList<String> referencedDataSource) {
		String[] teste = new String[] {};
		teste = referencedDataSource.toArray(teste);
		for (int i = teste.length - 1; i >= 1; i--) {
			for (int j = 0; j < i; j++) {
				if (teste[j].length() < teste[j + 1].length()) {
					String aux = teste[j];
					teste[j] = teste[j + 1];
					teste[j + 1] = aux;
				}
			}
		}
		return new ArrayList<String>(Arrays.asList(teste));
	}
}
