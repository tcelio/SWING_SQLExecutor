package com.indra.sql;

import java.util.ArrayList;

public class ScriptData {

	private SQLType scriptType;
	private String dataSource;
	private StringBuilder script;
	private ArrayList<String> referencedDataSource = new ArrayList<String>();

	public ScriptData(SQLType scriptType, String dataSource,
			StringBuilder script) {
		this.scriptType = scriptType;
		this.dataSource = dataSource;
		this.script = script;
		if (script != null) {
			String substring = script.toString();
			while (substring.length() > 0) {
				if (substring.toString().contains("&INGRID")) {
					int indexOf = substring.toString().indexOf("&INGRID");
					substring = substring.toString().substring(indexOf + 1,
							substring.toString().length());

					int last = substring.toString().length();
					if (substring.contains(".")) {
						last = substring.indexOf(".");
					}
					if (substring.contains("'")
							&& substring.indexOf("'") < last) {
						last = substring.indexOf("'");
					}
					if (substring != null) {
						String dataSourceReference = substring.substring(0,
								last);
						if (!referencedDataSource.contains(dataSourceReference)) {
							referencedDataSource.add(dataSourceReference);
						}
						substring = substring.substring(last,
								substring.length());
					}
				} else {
					break;
				}
			}
		}
	}

	public SQLType getScriptType() {
		return this.scriptType;
	}

	public String getDataSource() {
		return this.dataSource;
	}

	public StringBuilder getScript() {
		return this.script;
	}

	public void setScript(String script) {
		this.script = new StringBuilder(script);
	}

	public void setReferencedDataSource(String referencedDataSource) {
		this.referencedDataSource.add(referencedDataSource);
	}

	public ArrayList<String> getReferencedDataSource() {
		return this.referencedDataSource;
	}

}
