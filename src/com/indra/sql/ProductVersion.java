package com.indra.sql;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;

import com.indra.util.ExecutionMode;

public class ProductVersion {

	/**
	 * ID de registro de aplica��o da vers�o na base de dados.
	 */
	private Long versionAppliedID = 0L;

	/**
	 * Nome da aplica��o.
	 */
	// TODO: Implementar aqui o controle do nome da aplica��o e remover as
	// constantes no c�digo.
	private String appName = "";

	/**
	 * Numero da Vers�o
	 */
	private String version;

	/**
	 * Modo de Execu��o da vers�o
	 */
	private ExecutionMode executionMode;

	/**
	 * Companhia ou Grupo (ou Lista de Companhias ou de Grupos) onde o Script
	 * pode ser aplicado
	 */
	private ArrayList<String> companyWhereCanApply = new ArrayList<String>();

	/**
	 * Lista com os Scripts de Patch integrantes da vers�o
	 */
	private Map<Integer, ScriptFile> scriptPatchFiles = new HashMap<Integer, ScriptFile>();

	/**
	 * Lista com os Scripts de Rollback integrantes da vers�o
	 */
	private Map<Integer, ScriptFile> scriptRollbackFiles = new HashMap<Integer, ScriptFile>();

	/**
	 * Lista com os problemas e inconsist�ncias encontradas durante a valida��o
	 * do pacote de vers�o.
	 */
	private Map<String, ArrayList<Exception>> productVersionProblems = new HashMap<String, ArrayList<Exception>>();

	/**
	 * Seta o ID da aplica��o da vers�o gravada na base de dados (tabela
	 * ZEUS_VERSION_APPLIED)
	 * 
	 * @param id
	 *            ID a ser setado na tabela de controle de aplica��o de vers�o.
	 */
	public void setVersionAppliedID(Long id) {
		if (id != null) {
			versionAppliedID = id;
		}
	}

	/**
	 * Obt�m o ID da vers�o gravada na base de dados.
	 * 
	 * @return ID da vers�o gravada na base de dados.
	 */
	public Long getVersionAppliedID() {
		return versionAppliedID;
	}

	/**
	 * Obt�m o r�tulo da vers�o.
	 * 
	 * @return R�tulo da vers�o.
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * Seta o r�tulo da vers�o.
	 * 
	 * @param version
	 *            R�tulo da vers�o.
	 */
	public void setVersion(String version) {
		this.version = version;
	}

	/**
	 * Seta uma Companhia (ou Grupo) onde a Vers�o pode ser aplicada
	 * 
	 * @param companyList
	 *            Lista com os Nomes das Companhias ou Grupos
	 */
	public void setCompanyWhereCanApply(ArrayList<String> companyList) {
		if (companyList != null) {
			companyWhereCanApply = companyList;
		}
	}

	/**
	 * Adiciona uma Companhia (ou Grupo) onde a Vers�o pode ser aplicada
	 * 
	 * @param companyName
	 *            Nome da Companhia (ou Grupo)
	 */
	public void addCompanyWhereCanApply(String companyName) {
		if (companyName != null) {
			companyWhereCanApply.add(companyName.toUpperCase());
		}
	}

	/**
	 * Verifica se possui Defini��o de Execu��o da Vers�o em alguma Companhia ou
	 * Grupo espec�ficos.
	 * 
	 * @return Se possui ou n�o alguma Defini��o de Execu��o da Vers�o em alguma
	 *         Companhia ou Grupo espec�ficos
	 */
	public boolean hasCompanyDefinition() {
		return companyWhereCanApply.size() > 0;
	}

	/**
	 * Verifica se a Vers�o pode ser aplicado em uma determinada Companhia ou
	 * Grupo
	 * 
	 * @param companyName
	 *            Nome da Companhia ou Grupo a ser verificado
	 * @return Se a Vers�o pode ou n�o ser executado na Companhia ou Grupo
	 *         informado
	 */
	public Boolean canExecuteInCompany(String companyName) {
		if (companyName == null) {
			// Se a Compania n�o foi definida e se h� restri��o de
			// Companhias ou Grupos para a execu��o da Vers�o
			return companyWhereCanApply.size() == 0;
		} else {
			// Se a Companhia foi definido verifica se existe
			// restri��o de aplica��o da Vers�o e caso exista, se deve ser
			// aplicado na Companhia passada por par�metro
			if (companyWhereCanApply.size() == 0) {
				return false;
			} else {
				return companyWhereCanApply.contains(companyName.toUpperCase());
			}
		}
	}

	/**
	 * Adiciona um objeto contendo o conte�do do arquivo de script.
	 * 
	 * @param scriptFile
	 *            Objeto contendo o conte�do do arquivo de script de Patch.
	 * @param scriptRollbackFile
	 *            Objeto contendo o conte�do do arquivo de script de Rollback.
	 */
	public void putScriptFile(ScriptFile scriptPatchFile,
			ScriptFile scriptRollbackFile) {
		if (scriptPatchFile != null && scriptPatchFile.getFileName() != null) {
			scriptPatchFiles.put(scriptPatchFiles.size(), scriptPatchFile);
			scriptRollbackFiles.put(scriptRollbackFiles.size(),
					scriptRollbackFile);
		}
	}

	public Map<Integer, ScriptFile> getScriptPatchFiles() {
		return scriptPatchFiles;
	}

	/**
	 * Obt�m a lista de Scripts de Rollback na ordem em que dever�o ser
	 * aplicados (ordem inversa aos Scripts de Patch)
	 * 
	 * @return Scripts de Rollback ordenados.
	 */
	public Map<Integer, ScriptFile> getScriptRollbackFilesOrdened() {
		Map<Integer, ScriptFile> retVal = new HashMap<Integer, ScriptFile>();
		for (Integer index : scriptRollbackFiles.keySet()) {
			retVal.put((scriptRollbackFiles.size()-1) - index,
					scriptRollbackFiles.get(index));
		}
		return retVal;
	}

	public void reportProblem(ScriptFile scriptFile, Exception exception) {
		reportProblem(scriptFile.getFileName(), exception);
	}

	public void reportProblem(String filePath, Exception exception) {
		if (!productVersionProblems.containsKey(filePath)) {
			productVersionProblems.put(filePath, new ArrayList<Exception>());
		}
		productVersionProblems.get(filePath).add(exception);
	}

	public boolean hasProblem() {
		return productVersionProblems != null
				&& !productVersionProblems.isEmpty();
	}

	public void printProblems(Log log) {
		for (Entry<String, ArrayList<Exception>> entry : productVersionProblems
				.entrySet()) {
			for (Exception e : entry.getValue()) {
				log.error("\nRefer�ncia: " + entry.getKey() + "\nErro: "
						+ e.getClass() + "\n" + e.getLocalizedMessage());
			}
		}
	}

	public ArrayList<String> getRequiredDataSource() {
		ArrayList<String> retList = new ArrayList<String>();
		for (ScriptFile scriptFile : scriptPatchFiles.values()) {
			for (ScriptData scriptData : scriptFile.getScriptDataPool()
					.values()) {
				if (!retList.contains(scriptData.getDataSource())) {
					retList.add(scriptData.getDataSource());
				}
				for (String string : scriptData.getReferencedDataSource()) {
					if (!retList.contains(string)) {
						retList.add(string);
					}
				}
			}
		}
		return retList;
	}

	/**
	 * Obt�m o Modo de Execu��o realizado da Vers�o.
	 * 
	 * @return Enumerador com o Modo de Execu��o realizado
	 */
	public ExecutionMode getExecutionMode() {
		return executionMode;
	}

	/**
	 * Seta o Modo de Execu��o realizado da Vers�o.
	 * 
	 * @param executionMode
	 *            Enumerador com o Modo de Execu��o realizado
	 */
	public void setExecutionMode(ExecutionMode executionMode) {
		this.executionMode = executionMode;
	}
}
