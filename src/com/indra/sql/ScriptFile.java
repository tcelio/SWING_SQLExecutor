package com.indra.sql;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.indra.sql.exception.ScriptCommandException;

/**
 * Classe respons�vel por armazenar os Comandos SQL contidos em um arquivo de
 * Script .sql
 * 
 * @author rmarini
 * 
 */
public class ScriptFile {

	/**
	 * Nome do Arquivo
	 */
	private String fileName;

	/**
	 * Caminho at� o arquivo a partir do path do execute.txt
	 */
	private String fileNameFromExecute;
	/**
	 * Companhia ou Grupo (ou Lista de Companhias ou de Grupos) onde o Script
	 * pode ser aplicado
	 */
	private ArrayList<String> companyWhereCanApply = new ArrayList<String>();
	/**
	 * Lista com os Comandos SQL contidos no Arquivo
	 */
	private Map<Integer, ScriptData> scriptDataPool = new HashMap<Integer, ScriptData>();

	private Exception executionException;

	/**
	 * Define o nome do Arquivo
	 * 
	 * @param fileName
	 *            Nome do Arquivo
	 */
	public ScriptFile(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * Obt�m o nome do Arquivo de Scripts
	 * 
	 * @return Nome do Arquivo de Scripts
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * Obt�m o caminho do arquivo a partir da raiz da vers�o.
	 * 
	 * @return String com o caminho do arquivo a partir da raiz da vers�o.
	 */
	public String getFileNameFromExecute() {
		return fileNameFromExecute;
	}

	/**
	 * Seta o caminho do arquivo a partir da raiz da vers�o.
	 * 
	 * @param fileNameFromExecute
	 *            String com o caminho do arquivo a partir da raiz da vers�o.
	 */
	public void setFileNameFromExecute(String fileNameFromExecute) {
		this.fileNameFromExecute = fileNameFromExecute;
	}

	/**
	 * Seta uma Companhia (ou Grupo) onde o Script pode ser aplicado
	 * 
	 * @param companyList
	 *            Lista com os Nomes das Companhias ou Grupos
	 */
	public void setCompanyWhereCanApply(ArrayList<String> companyList) {
		if (companyList != null) {
			companyWhereCanApply = companyList;
		}
	}

	/**
	 * Adiciona uma Companhia (ou Grupo) onde o Script pode ser aplicado
	 * 
	 * @param companyName
	 *            Nome da Companhia (ou Grupo)
	 */
	public void addCompanyWhereCanApply(String companyName) {
		if (companyName != null) {
			companyWhereCanApply.add(companyName.toUpperCase());
		}
	}

	/**
	 * Verifica se possui Defini��o de Execu��o do Script em alguma Companhia ou
	 * Grupo espec�ficos.
	 * 
	 * @return Se possui ou n�o alguma Defini��o do Script Vers�o em alguma
	 *         Companhia ou Grupo espec�ficos
	 */
	public boolean hasCompanyDefinition() {
		return companyWhereCanApply.size() > 0;
	}

	/**
	 * Verifica se o Script pode ser aplicado em uma determinada Companhia ou
	 * Grupo
	 * 
	 * @param companyName
	 *            Nome da Companhia ou Grupo a ser verificado
	 * @return Se o script pode ou n�o ser executado na Companhia ou Grupo
	 *         informado
	 */
	public Boolean canExecuteInCompany(String companyName) {
		if (companyName == null) {
			// Se a Compania n�o foi definida e se h� restri��o de
			// Companhias ou Grupos para a execu��o do script...
			return companyWhereCanApply.size() == 0;
		} else {
			// Se a Companhia foi definido verifica se existe
			// restri��o de aplica��o do script e caso exista, se deve ser
			// aplicado na Companhia passada por par�metro
			return companyWhereCanApply.contains(companyName.toUpperCase());
		}
	}

	/**
	 * Adiciona um Comando SQL ao Script
	 * 
	 * @param dataSource
	 *            Nome do DataSource que dever� ser utilizado na execu��o do
	 *            Comando SQL
	 * @param sqlCommand
	 *            Sctring contendo o Comando SQL
	 * @throws ScriptCommandException
	 *             Exception de valida��o de Comando SQL
	 */
	public void addScriptCommand(String dataSource, StringBuilder sqlCommand)
			throws ScriptCommandException {
		if (sqlCommand != null) {
			SQLType scriptType = getSQLType(sqlCommand.toString());
			if (scriptType != null) {
				ScriptData scriptData = new ScriptData(scriptType, dataSource,
						sqlCommand);
				scriptDataPool.put(scriptDataPool.size(), scriptData);
			} else {
				throw new ScriptCommandException(
						ScriptCommandException.SQL_TYPE_NOT_IDENTIFYED,
						"N�o foi poss�vel definir o Tipo de Script");
			}
		}
	}

	/**
	 * Obt�m a Lista de Comandos SQL contidos no Script
	 * 
	 * @return Lista de Comandos SQL contidos no Script
	 */
	public Map<Integer, ScriptData> getScriptDataPool() {
		return scriptDataPool;
	}

	@Override
	public boolean equals(Object obj) {
		// Alterar para comparar apenas o nome do arquivo
		return super.equals(obj);
	}

	/**
	 * Obt�m o tipo de Comando SQL: <tt>DML</tt> ou <tt>DDL</tt>
	 * 
	 * @param sqlCommand
	 *            Comando SQL
	 * @return O tipo de Comando SQL: <tt>DML</tt> ou <tt>DDL</tt>
	 */
	private SQLType getSQLType(String sqlCommand) {
		if (sqlCommand.trim().toUpperCase().startsWith("INSERT")
				|| sqlCommand.trim().toUpperCase().startsWith("UPDATE")
				|| sqlCommand.trim().toUpperCase().startsWith("DELETE")
				|| sqlCommand.trim().toUpperCase().startsWith("COMMIT")) {
			return SQLType.DML;
		} else {
			return SQLType.DDL;
		}
	}

	/**
	 * Seta uma exce��o de execu��o do script.
	 * 
	 * @param executionException
	 *            Exce��o ocorrida ao executar o script na base de dados.
	 */
	public void setExecutionException(Exception executionException) {
		this.executionException = executionException;
	}

	/**
	 * Obt�m exce��o caso tenha ocorrido durante a execu��o do script.
	 * 
	 * @return Exce��o ocorrida ao executar o script na base de dados.
	 */
	public Exception getExecutionException() {
		return executionException;
	}
}
