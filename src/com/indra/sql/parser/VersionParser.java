package com.indra.sql.parser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.channels.ShutdownChannelGroupException;
import java.util.ArrayList;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.indra.sql.ProductVersion;
import com.indra.sql.ScriptFile;
import com.indra.util.ExecutionMode;
import com.indra.util.IStringConstants;
import com.indra.util.swing.SwingEventsManager;

public class VersionParser {

	private static final Log log = LogFactory.getLog(VersionParser.class);

	private String currentSchema = "";
	private int flagLayout;

	private ScriptFileParser fileParser = new ScriptFileParser();

	public VersionParser(){
	}
	
	public VersionParser(int version){ //1 - swing | 0 - prompt
		this.flagLayout = version;
	}
	
	
	public ArrayList<ProductVersion> loadVersion(File fileOrDirectory, int flagLayoutType) {
		this.flagLayout = flagLayoutType;
		ArrayList<ProductVersion> retVal = new ArrayList<ProductVersion>();
		if (fileOrDirectory.isDirectory()) {
			VersionParser versionParser = new VersionParser();
			
			String executeFile = versionParser.verifyExecutionFileNumber(fileOrDirectory);
			boolean verifyFile = analiseExecuteFile(executeFile);
			if(!executeFile.isEmpty() && verifyFile){
				for (File file : fileOrDirectory.listFiles()) {
					if (file.isFile()) {
						if (file.getName() != null
								&& file.getName()
										.toUpperCase()
										.startsWith(
												IStringConstants.EXECUTION_FILE_PREFIX)) {
							try {
								retVal = loadVersionFromExecutionFile(file);
							} catch (FileNotFoundException e) {
								if(flagLayout == 1){
									SwingEventsManager.showException(e, "Arquivo n�o encontrado.");
								}
								log.warn("Arquivo n�o encontrado: "
										+ fileOrDirectory.getAbsolutePath());
							} catch (IOException e) {
								if(flagLayout == 1){
									SwingEventsManager.showException(e, "Arquivo n�o encontrado.");
								}
								log.warn("Arquivo n�o encontrado: "
										+ fileOrDirectory.getAbsolutePath());
							}
						}
					}
				}
			}

		} else {
			if (fileOrDirectory.getName() != null
					&& fileOrDirectory.getName().toUpperCase()
							.startsWith(IStringConstants.EXECUTION_FILE_PREFIX)) {
				try {
					ArrayList<ProductVersion> productList = loadVersionFromExecutionFile(fileOrDirectory);
					if (productList != null && !productList.isEmpty()) {
						//productList.   setExecutionMode(ExecutionMode.PATCH);
						retVal.addAll(productList);
					}
				} catch (FileNotFoundException e) {
					if(flagLayout == 1){
						SwingEventsManager.showException(e, "Arquivo n�o encontrado: "+fileOrDirectory.getName());
					}
					log.warn("Arquivo n�o encontrado: "
							+ fileOrDirectory.getAbsolutePath());
				} catch (IOException e) {
					if(flagLayout == 1){
						SwingEventsManager.showException(e, "Arquivo n�o encontrado: "+fileOrDirectory.getName());
					}
					log.warn("Arquivo n�o encontrado: "
							+ fileOrDirectory.getAbsolutePath());
				}
			}
		}
		
		
		
		return setProductVersionRollBack(retVal);
	}
	/**
	 * Metodo para verificar se o arquivo EXECUTE.txt est� presente e a quantidade deste arquivo.
	 * Necess�rio existir apenas um arquivo 'execute'. 
	 */
	public String verifyExecutionFileNumber(File fileOrDirectory){
		String filePath = null;
		int fileNum = 0;
		try {
			if (fileOrDirectory.isDirectory()){
				for (File file : fileOrDirectory.listFiles()) {
					if (file.isFile()) {
						if (file.getName() != null
								&& file.getName()
										.toUpperCase()
										.startsWith(
												IStringConstants.EXECUTION_FILE_PREFIX)) {
							fileNum ++;
							filePath = file.getAbsolutePath();
						}
					}
				}
			}
			if(fileNum > 1){
				if(flagLayout == 1){
					SwingEventsManager.showExceptionStr("Erro: H� mais de um arquivo de refer�ncia 'execute' no caminho especificado.");
				}
			}
			if(fileNum == 0){
				if(flagLayout == 1){
					SwingEventsManager.showExceptionStr("Erro: N�o h� arquivops de refer�ncia 'execute' no caminho especificado.");
				}
			}else{
				
			}
				
			
		} catch (Exception e) {
			if(flagLayout == 1){
				SwingEventsManager.showException(e, "Falha ao localizar o arquivo de referencia 'execute'!");
			}
			log.error("Falha ao localizar o arquivo de referencia 'execute' ");
		}
		return filePath;
	}
	
	
	
	public boolean analiseExecuteFile(String fileStr){
		boolean analise = false;
		File file = new File(fileStr);
		try {
			FileReader fr = null;
			String s = new String();
			try {
				if (!file.exists()) {
					if(this.flagLayout == 1){
						SwingEventsManager.showExceptionStr("Arquivo n�o Encontrado: "
							+ file.getCanonicalPath().toString());
					}
					throw new FileNotFoundException("Arquivo n�o Encontrado: "
							+ file.getCanonicalPath().toString());
					
				} else {
					log.info("Encontrado Arquivo de Execu��o: " + file);
				}

				String executionFileRoot = file.getParent().replace("\\", "/")
						+ "/";

				fr = new FileReader(file);

				BufferedReader br = new BufferedReader(fr);

				// Indica que � um arquivo principal que concentra a execu��o de
				// outras vers�es
				Boolean isMultPathExecutionFile = false;

				while ((s = br.readLine()) != null) {
					if (s != null && s.trim().length() > 0
							&& !s.trim().startsWith("--")) {
						
						if((s.trim().contains(".SQL") || s.trim().contains(".sql")) ||
								(verifyFileType(s,
										IStringConstants.EXECUTION_FILE_PREFIX))){
							analise = true;
						}
					}
				}
				
				br.close();
			} catch (IOException e) {
				log.error(e);
			} finally {
				if (fr != null) {
					try {
						fr.close();
					} catch (Exception e2) {
						// Se n�o conseguir fechar o arquivo, n�o tem problema.
					}
				}
			}
		} catch (Exception e) {
			
		}
		
		return analise;
	}
	
	
	public static ArrayList<ProductVersion> setProductVersionRollBack(ArrayList<ProductVersion> versionsList){
		
		ArrayList<ProductVersion> productVersion = new ArrayList<ProductVersion>();
		
		for (ProductVersion plist : versionsList) {
			plist.setExecutionMode(ExecutionMode.ROLLBACK);
			productVersion.add(plist);
		}
		
		return productVersion;
		
	}
	
	
	public String loadVersionRollback(File diretorio, ArrayList<String> pathList) {
		
		//ArrayList<ProductVersion> retVal = new ArrayList<ProductVersion>();
		String rollbackVersion = null;
		String rollbackVersion_ = null;
		String version = null;
		String module = null;
		
		try {
			
			if(diretorio.isDirectory() && diretorio.exists()){
				for(File file :diretorio.listFiles()){//listar o diretorio principal
					
					
					ArrayList<String> paths = new ArrayList<String>();
					//path version
					if(file.isDirectory()){
						version = file.getName();
						System.out.println("..."+file);
						for(File f : file.listFiles()){//listar a versao
							
							//System.out.println("..."+f);

							//diretorio	de modulo
							if(f.isDirectory()){
								
								module = f.getName().toString();
								//System.out.println("..->>"+module);
								for(File g : f.listFiles()){//listar o modulo
									//System.out.println("..."+g);
									
										if(g.isDirectory() && g.getName().matches("Patch")){
											for(File script : g.listFiles()){//listar o patch
												//System.out.println("..->"+script);
												//System.out.println("..->"+script.getPath());
												//System.out.println("..->"+script.getAbsolutePath());
												//System.out.println("..->"+script.getCanonicalPath());
												
												int i = script.toString().indexOf(module);
												String script_ = script.toString().substring(i, script.toString().length());
												rollbackVersion_ = script.toString().substring(0, i);
												String script_b = script_.replace("\\", "/");
												
												if(pathList.contains(script_b)){
													paths.add(script_b);
												}
											}
										}
										
									
								}
							}
							
						}
						if(paths.containsAll(pathList)){
							rollbackVersion = rollbackVersion_; 

						}

					}
				}
			}
			
			
			
			
		} catch (Exception e) {
			if(flagLayout == 1){
				SwingEventsManager.showException(e, "Falha ao obter a vers�o correta de rollback.\nVerificar caminho inserido no Rollback.");
			}
			log.error("Error...");
		}
		
		
		
		return rollbackVersion;
	}
	private ArrayList<ProductVersion> loadVersionFromExecutionFile(File file)
			throws FileNotFoundException, IOException {
		ArrayList<ProductVersion> productVersions = new ArrayList<ProductVersion>();
		String s = new String();
		ProductVersion version = new ProductVersion();

		FileReader fr = null;
		try {
			if (!file.exists()) {
				throw new FileNotFoundException("Arquivo n�o Encontrado: "
						+ file.getCanonicalPath().toString());
			} else {
				log.info("Encontrado Arquivo de Execu��o: " + file);
			}

			String executionFileRoot = file.getParent().replace("\\", "/")
					+ "/";

			fr = new FileReader(file);

			BufferedReader br = new BufferedReader(fr);

			// Indica que � um arquivo principal que concentra a execu��o de
			// outras vers�es
			Boolean isMultPathExecutionFile = false;

			while ((s = br.readLine()) != null) {
				if (s != null && s.trim().length() > 0
						&& !s.trim().startsWith("--")) {
					if (verifyFileType(s,
							IStringConstants.EXECUTION_FILE_PREFIX)) {
						log.info("Arquivo de execu��o encontrado: " + s);
						try {
							isMultPathExecutionFile = true;
							ArrayList<ProductVersion> tempProducVersions = loadVersionFromExecutionFile(new File(
									executionFileRoot + s));
							if (tempProducVersions != null
									&& !tempProducVersions.isEmpty()) {
								productVersions.addAll(tempProducVersions);
							}
						} catch (FileNotFoundException e) {
							if(flagLayout == 1){
								SwingEventsManager.showException(e, "Arquivo de execu��o n�o encontrado.");
							}
							version.reportProblem(s, e);
							log.warn("Arquivo n�o encontrado: " + s);
						}
						
						
					} else if (s.toUpperCase().contains(
							IStringConstants.COMPANY_WHERE_CAN_APPLY)) {
						String Company = s.replace(
								IStringConstants.COMPANY_WHERE_CAN_APPLY + "=",
								"");
						String[] CompanyNameSplited = Company.split(",");
						for (String companyName : CompanyNameSplited) {
							version.addCompanyWhereCanApply(companyName);
						}
						log.info("Definindo Lista de Companhias onde a Vers�o pode ser Executada: ["
								+ Company + "]");
						
						
					} else if (s.toUpperCase().contains(
							IStringConstants.INGRID_VERSION_TAG)) {
						String versionString = s.replace(
								IStringConstants.INGRID_VERSION_TAG, "");
						version.setVersion(versionString);
						log.info("Definindo Vers�o: " + versionString);
						
					} else if (s.toUpperCase().contains(
							IStringConstants.SCHEMA_CONN_TAG)) {
						currentSchema = s.substring(
								IStringConstants.SCHEMA_CONN_TAG.length() + 1,
								s.length());
						log.info("Definindo Schema: " + currentSchema);
						
					} else if (verifyFileType(s, IStringConstants.SQL_FILE_NAME)) {
						ScriptFile parseSQLFile = null;
						ScriptFile parseRollbackSQLFile = null;
						String fullFilePath = executionFileRoot + s;
						log.info("Lendo arquivo SQL: " + fullFilePath);
						try {
							parseSQLFile = fileParser.parseSQLFile(
									currentSchema, new File(fullFilePath));
							parseSQLFile.setFileNameFromExecute(s);
						} catch (FileNotFoundException e) {
							if(flagLayout == 1){
								SwingEventsManager.showException(e, "Falha ao obter informa��o de scripts SQL.");
							}
							version.reportProblem(fullFilePath, e);
							log.warn("Arquivo n�o encontrado: " + fullFilePath);
						}
						// Verifica��o do script de Rollback
						s = getRollbackPath(s);
						fullFilePath = executionFileRoot + s;
						log.info("Verificando arquivo SQL de Rollback: "
								+ fullFilePath);
						try {
							parseRollbackSQLFile = fileParser.parseSQLFile(
									currentSchema, new File(fullFilePath));
							parseRollbackSQLFile.setFileNameFromExecute(s);
						} catch (FileNotFoundException e) {
							if(flagLayout == 1){
								SwingEventsManager.showException(e,"");
							}
							version.reportProblem(fullFilePath, e);
							log.warn("Arquivo n�o encontrado: " + fullFilePath);
						}
						version.putScriptFile(parseSQLFile,
								parseRollbackSQLFile);
					}	
					
					else {
						FileNotFoundException e = new FileNotFoundException(
								"Caminho de Arquivo n�o reconhecido: " + s);
						version.reportProblem(s, e);
						log.warn("Caminho de Arquivo n�o reconhecido: " + s);
					}
				}
			}
			if (!isMultPathExecutionFile && version != null) {
				if (version.getVersion() == null
						|| version.getVersion().isEmpty()) {
					log.error("N�o definida tag \""
							+ IStringConstants.INGRID_VERSION_TAG
							+ "=\" no inicio do arquivo " + file);
				} else {
					productVersions.add(version);
				}
			}
			br.close();
		} catch (IOException e) {
			if(flagLayout == 1){
				SwingEventsManager.showException(e, "Falha ao caregar informa��es do arquivo de execu��o.");
			}
			log.error(e);
		} finally {
			if (fr != null) {
				try {
					fr.close();
				} catch (Exception e2) {
					// Se n�o conseguir fechar o arquivo, n�o tem problema.
				}
			}
		}
		return productVersions;
	}

	
	
	
	/**
	 * Repassa o padr�o de localiza��o do script de Rollback do script de Patch.
	 * 
	 * @param path
	 *            Path do script de Patch
	 * @return Path para o script de Rollback correspondente ao script de Patch.
	 */
	private String getRollbackPath(String path) {
		return path.replaceAll("(?i)patch", "Rollback").replaceAll("(?i).sql",
				"_Rollback.sql");
	}

	private boolean verifyFileType(String filePath, String fileNameCompare) {
		if (filePath.length() > fileNameCompare.length()
				&& filePath.toUpperCase().contains(fileNameCompare))
			return true;
		else
			return false;
	}



}
