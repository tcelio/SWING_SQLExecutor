package com.indra.sql.parser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.indra.sql.ScriptFile;
import com.indra.sql.exception.ScriptCommandException;
import com.indra.util.FileEncodingDetector;
import com.indra.util.IStringConstants;

public class ScriptFileParser {

	private static final Log log = LogFactory.getLog(ScriptFileParser.class);

	String dataSource = "INGRID";

	// TODO acertar as tratativas para valida��o do caminho do arquivo e
	// retornar exce��o
	/**
	 * Executa a leitura do Arquivo contendo os Comandos SQL.
	 * 
	 * @param dataSource
	 *            String da Conex�o com o Banco de Dados
	 * @param sqlFile
	 *            Arquivo contendo os Comandos SQL
	 * @return Objeto <code>ScriptFile</code> preenchido com os dados
	 * @throws FileNotFoundException
	 *             Exception de Arquivo n�o encontrado
	 * @throws IOException
	 *             Exception de Acesso ao Arquivo
	 */
	public ScriptFile parseSQLFile(String dataSource, File sqlFile)
			throws FileNotFoundException, IOException {
		ScriptFile retVal = null;

		if (dataSource != null && !dataSource.trim().isEmpty()) {
			this.dataSource = dataSource;
		}
		String s = new String();
		StringBuilder sb = new StringBuilder();

		if (sqlFile == null || !sqlFile.exists())
			throw new FileNotFoundException("Arquivo n�o encontrado: "
					+ sqlFile.toString());

		String encoding = FileEncodingDetector.detectEncoding(sqlFile);
		FileInputStream fr = null;
		try {
			fr = new FileInputStream(sqlFile);
			InputStreamReader inputStreamReader = new InputStreamReader(fr,
					encoding);
			BufferedReader br = new BufferedReader(inputStreamReader);

			while ((s = br.readLine()) != null) {
				sb.append(s + "\n");
			}
			br.close();

			retVal = generateScriptFile(sqlFile.getName(), sb);

		} catch (IOException e) {
			// TODO tratar exception
			e.printStackTrace();
		} finally {
			if (fr != null) {
				try {
					fr.close();
				} catch (Exception e2) {
					// Se n�o conseguir fechar o arquivo, n�o tem problema.
				}
			}
		}
		return retVal;
	}

	/**
	 * Gera um Objeto do tipo <code>com.indra.sql.ScriptFile</code> com os dados
	 * preenchidos.
	 * 
	 * @param fileName
	 *            Nome do Arquivo originario
	 * @param fileContent
	 *            Conte�do do Arquivo
	 * @return Objeto do tipo <code>com.indra.sql.ScriptFile</code> com os dados
	 *         preenchidos
	 */
	private ScriptFile generateScriptFile(String fileName,
			StringBuilder fileContent) {

		String splitString = identifySplitString(fileContent);

		String[] inst = fileContent.toString().split(splitString);

		ScriptFile scriptFile = new ScriptFile(fileName);

		for (int i = 0; i < inst.length; i++) {
			if (!inst[i].trim().equals("")) {
				StringBuilder sqlCommand = new StringBuilder(inst[i].trim());
				log.debug(">>" + sqlCommand);
				verifyCompanyDefinition(sqlCommand, scriptFile);
				verifyDataSourceDefinition(sqlCommand);
				removeUnnecessarySpace(sqlCommand);
				try {
					scriptFile.addScriptCommand(this.dataSource,
							new StringBuilder(sqlCommand));
				} catch (ScriptCommandException e) {
					log.warn(e);
				}
			}
		}
		return scriptFile;
	}

	/**
	 * Identifica se � um bloco PLSQL.
	 * 
	 * @param sb
	 *            StringBuffer com o conte�do SQL
	 * @return "/" caso seja um bloco PLSQL ou ";" caso seja um bloco simples;
	 */
	private String identifySplitString(StringBuilder sb) {
		String retVal = ";";
		if (sb != null && sb.length() > 0) {
			for (String plsql : IStringConstants.PLSQL_BLOCK_PREFIX) {
				if (sb.toString().toUpperCase().contains(plsql)) {
					retVal = "\n/\n";
					break;
				}
			}
		}
		return retVal;
	}

	/**
	 * Verifica se o Comando SQL possui Defini��o de Companhia ou Grupo.
	 * 
	 * @param sqlCommand
	 *            String contendo o Comando SQL
	 * @param scriptFile
	 *            ScriptFile ao qual o Comando SQL pertence
	 */
	private void verifyCompanyDefinition(StringBuilder sqlCommand,
			ScriptFile scriptFile) {
		if (sqlCommand.toString().trim().toUpperCase()
				.startsWith(IStringConstants.COMPANY_WHERE_CAN_APPLY)) {
			String companyDefinition = sqlCommand.substring(0,
					sqlCommand.indexOf("\n"));

			String replacedCommand = sqlCommand.toString().replace(
					companyDefinition, "");
			sqlCommand.setLength(0);
			sqlCommand.append(replacedCommand);

			companyDefinition = companyDefinition.replace(
					IStringConstants.COMPANY_WHERE_CAN_APPLY + "=", "");

			scriptFile
					.setCompanyWhereCanApply(getCompanyWhereCanApply(companyDefinition
							.trim().toUpperCase()));
		}
	}

	/**
	 * Verifica se o Comando SQL possui Defini��o de Conex�o com DataSource.
	 * 
	 * @param sqlCommand
	 *            String contendo o Comando SQL
	 */
	private void verifyDataSourceDefinition(StringBuilder sqlCommand) {
		if (sqlCommand.toString().trim().toUpperCase()
				.startsWith(IStringConstants.SCHEMA_CONN_TAG)) {
			String connectionDefinition = sqlCommand.substring(0,
					sqlCommand.indexOf("\n"));

			String replacedCommand = sqlCommand.toString().replace(
					connectionDefinition, "");

			sqlCommand.setLength(0);
			sqlCommand.append(replacedCommand);

			this.dataSource = connectionDefinition.replace(
					IStringConstants.SCHEMA_CONN_TAG, "").trim();
		}
	}

	/**
	 * Obt�m uma lista (<code>ArrayList</code>) de Companhias ou Grupos a partir
	 * de uma String.
	 * 
	 * @param companyDefinition
	 *            String com a Defini��o das Companhias ou Grupos
	 * @return <code>ArrayList</code> com as Companhias ou Grupos
	 */
	private ArrayList<String> getCompanyWhereCanApply(String companyDefinition) {
		ArrayList<String> companyList = new ArrayList<String>();
		String[] split = companyDefinition.split(",");

		if (split != null) {
			for (String companyName : split) {
				companyList.add(companyName);
			}
		}
		return companyList;
	}

	/**
	 * Remove espa�os desnecess�rios no inicio e no fim do Comando SQL.
	 * 
	 * @param sqlCommand
	 *            Comando SQL
	 */
	private void removeUnnecessarySpace(StringBuilder sqlCommand) {
		String trim = sqlCommand.toString().trim();
		sqlCommand.setLength(0);
		sqlCommand.append(trim);
	}
}
