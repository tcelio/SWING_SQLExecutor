package com.indra.sql.exception;

import java.io.Serializable;

public class ScriptCommandException extends Exception implements Serializable {

	private static final long serialVersionUID = -2640946990327149105L;

	public static final int NULL_VALUE = 1;
	public static final int SQL_TYPE_NOT_IDENTIFYED = 2;

	private int error = 0;
	private String causeOfException;

	public ScriptCommandException(int error, String causeOfException) {
		super();
		this.error = error;
		this.causeOfException = causeOfException;
	}

	public int getError() {
		return error;
	}

	public String getCauseOfException() {
		return causeOfException;
	}

}
