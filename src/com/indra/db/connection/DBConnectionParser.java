package com.indra.db.connection;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.indra.util.IStringConstants;
import com.indra.util.PropertyUtil;

public class DBConnectionParser {

	private static final Log log = LogFactory.getLog(DBConnectionParser.class);

	public DBConnectionManager getDBConnectionManager() {

		DBConnectionManager connectionManager = new DBConnectionManager();

		Properties prop = null;
		String urlAux = IStringConstants.ENVIRONMENT_PROPERTIES_FILE;

		try {
			String config = System.getProperty("config.dir");
			if (config != null) {
				urlAux = config + "/"
						+ IStringConstants.ENVIRONMENT_PROPERTIES_FILE;
			}
			prop = new PropertyUtil().getProperties(urlAux);
		} catch (IOException e) {
			log.error("Erro ao ler o arquivo de configuração "
					+ IStringConstants.ENVIRONMENT_PROPERTIES_FILE + ": " + e);
		}

		if (prop != null && !prop.isEmpty()) {

			Enumeration<?> e = prop.propertyNames();
			while (e.hasMoreElements()) {
				String dataSource = (String) e.nextElement();
				String connectionString = prop.getProperty(dataSource);

				if (dataSource != null && connectionString != null) {
					log.info("Carregando o DataSource " + dataSource);
					connectionManager.addConnection(dataSource,
							connectionString);
				}
			}
		}
		return connectionManager;
	}

}
