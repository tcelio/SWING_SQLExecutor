package com.indra.db.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.indra.db.DBConnection;

public class DBConnectionManager {

	private static final Log log = LogFactory.getLog(DBConnectionManager.class);

	Map<String, DBConnection> dbConnectionPool = new HashMap<String, DBConnection>();

	public void addConnection(String dataSource, String connectionString) {
		if (dataSource != null && connectionString != null) {

			DBConnection dbConnection = createDBConnection(dataSource,
					connectionString);
			dbConnectionPool.put(dataSource, dbConnection);
		}
	}

	public DBConnection createDBConnection(String dataSource,
			String completeConnString) {
		DBConnection dbConnection = new DBConnection();
		dbConnection.setDataSource(dataSource);
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");

			String[] completeConnStringSplit = completeConnString.split("/");
			String connectionString = completeConnStringSplit[0];
			String userAndPass = completeConnStringSplit[1];

			String[] userPassSplit = userAndPass.split(":");
			String userString = userPassSplit[0];
			String userPass = userPassSplit[1];

			dbConnection.setSchemaName(userString);
			dbConnection.setConnectionString(completeConnString);

			Connection connection = DriverManager.getConnection(
					connectionString, userString, userPass);

			dbConnection.setConnection(connection);

		} catch (ClassNotFoundException e) {
			log.warn("Falha ao conectar com o DataSource " + dataSource);
			dbConnection.reportException(e);
		} catch (SQLException e) {
			log.warn("Falha ao conectar com o DataSource " + dataSource);
			dbConnection.reportException(e);
		}
		return dbConnection;
	}

	public Boolean hasDataSource(String dataSource) {
		return dbConnectionPool.containsKey(dataSource);
	}

	public DBConnection getConnection(String dataSource) {
		DBConnection ret = null;
		if (dbConnectionPool.containsKey(dataSource)) {
			ret = dbConnectionPool.get(dataSource);
		}
		return ret;
	}

	public DBConnection retryMakeConnection(String dataSource) {
		DBConnection dbConnection = null;
		if (dbConnectionPool.containsKey(dataSource)) {
			dbConnection = createDBConnection(dataSource,
					dbConnectionPool.get(dataSource).getConnectionString());
			dbConnectionPool.put(dataSource, dbConnection);
		}
		return dbConnection;
	}

	public Boolean hasConnectionError() {
		for (Map.Entry<String, DBConnection> map : dbConnectionPool.entrySet()) {
			if (map.getValue() == null || map.getValue().getException() != null) {
				return Boolean.TRUE;
			}
		}
		return Boolean.FALSE;
	}

	public void printDBConnectionError() {
		for (Map.Entry<String, DBConnection> map : dbConnectionPool.entrySet()) {
			if (map.getValue() == null && !map.getValue().isConnected()) {
				log.error("\nErro na conex�o com o DataSource "
						+ map.getValue().getDataSourceName() + "\nErro: "
						+ map.getValue().getException().getClass() + "\n"
						+ map.getValue().getException().getLocalizedMessage());
			}
		}
	}
}
