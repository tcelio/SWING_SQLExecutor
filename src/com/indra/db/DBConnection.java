package com.indra.db;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class DBConnection {

	private static final Log log = LogFactory.getLog(DBConnection.class);

	private String schemaName;

	private String dataSourceName;

	private String connectionString;

	private Connection connection;

	private Exception exception;

	public DBConnection() {

	}

	public void setConnection(Connection connection) {
		this.connection = connection;
	}

	public void setDataSource(String dataSource) {
		this.dataSourceName = dataSource;
	}

	public void setConnectionString(String connectionString) {
		this.connectionString = connectionString;
	}

	public void setSchemaName(String schemaName) {
		this.schemaName = schemaName;
	}

	public Boolean isConnected() {
		try {
			return connection != null && !connection.isClosed();
		} catch (SQLException e) {
			log.error(e);
		}
		return false;
	}

	public String getDataSourceName() {
		return dataSourceName;
	}

	public String getConnectionString() {
		return this.connectionString;
	}

	public String getSchemaName() {
		return (this.schemaName != null ? this.schemaName : "").toUpperCase();
	}

	public Connection getConnection() {
		return this.connection;
	}

	public void reportException(Exception exception) {
		this.exception = exception;
	}

	public Exception getException() {
		return this.exception;
	}

}
