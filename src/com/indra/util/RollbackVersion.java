package com.indra.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.indra.util.swing.EventBean;
import com.indra.util.swing.TestRollback;

public class RollbackVersion {

	
	private static final Log log = LogFactory.getLog(TestRollback.class);
	private static Map<String, String> rollbackList = new HashMap<String, String>();
	private static List<EventBean> eventlist = new ArrayList<EventBean>();
	private static EventBean lastEvent = new EventBean();

	public RollbackVersion(){
		
	}
	
	
	public static void main(String[] args) {
			
		Collections.reverse(eventlist);
		boolean validator = false;
		for(EventBean b : eventlist){

			if(b.getType().matches("ROLLBACK")){
				addRollbackValue(b.getType(), b.getVersion());
				log.info("inserido no rollback -> "+b.getVersion());
			}
			if(b.getType().matches("PATCH") && !containVersion(b.getVersion())){
				if(!validator){
					lastEvent.setId(b.getId());
					lastEvent.setVersion(b.getVersion());
					lastEvent.setType("PATCH");
					log.info("inserido no last eventBean-> "+b.getVersion());
					validator = true;
				}
			}
		}
		mostrarVersao();
		listRollbacks();
	}

	public static void mostrarVersao(){
		System.out.println("versao atual eh: "+lastEvent.getVersion()+" ID: "+lastEvent.getId());
	}
	
	public static void addRollbackValue(String type, String version){
		rollbackList.put(version, type);
	}
	
	public static void addEventValue(EventBean event){
		eventlist.add(event);
	}
	
	public static boolean containVersion(String version){
		return rollbackList.containsKey(version);
	}
		
	public static void listRollbacks(){
		
		try {
			Iterator iterator = rollbackList.entrySet().iterator();
			
			while(iterator.hasNext()){
				Map.Entry map = (Map.Entry) iterator.next();
				System.out.println("TYPE: "+map.getKey());
				System.out.println("VERSION: "+map.getValue());
			}
			
		} catch (Exception e) {
			System.out.println("Error: "+e.getCause());
		}
		
	}

	public static void listEvents(){
		
		try {
			for(EventBean r : eventlist){
				System.out.println("ID: "+r.getId());
				System.out.println("TYPE: "+r.getType());
				System.out.println("VERSION: "+r.getVersion());
			}
		} catch (Exception e) {
			System.out.println("Error:: "+e.getCause());
		}
		
	}
	
	
	

/*		RollbackBean r1 = new RollbackBean();
		r1.setId(1);
		r1.setVersion("1.0");
				
		RollbackBean r2 = new RollbackBean();
		r2.setId(2);
		r2.setVersion("1.2");
		
		RollbackBean r3 = new RollbackBean();
		r3.setId(3);
		r3.setVersion("1.3");
		
		RollbackBean r4 = new RollbackBean();
		r4.setId(4);
		r4.setVersion("2.0");
		
		RollbackBean r5 = new RollbackBean();
		r5.setId(5);
		r5.setVersion("2.1");
		
		RollbackBean r6 = new RollbackBean();
		r6.setId(6);
		r6.setVersion("2.2");
		
		RollbackBean r7 = new RollbackBean();
		r7.setId(7);
		r7.setVersion("2.3");
	
		addRollbackValue(r1);addRollbackValue(r2);addRollbackValue(r3);
		addRollbackValue(r4);addRollbackValue(r5);addRollbackValue(r6);
		addRollbackValue(r7);
*/	
	
	
}
