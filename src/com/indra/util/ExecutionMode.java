package com.indra.util;

/**
 * Modo de execu��o dos Scripts.
 * 
 * @author rmarini
 * 
 */
public enum ExecutionMode {
	/**
	 * Modo de Execu��o de Patch.
	 */
	PATCH,
	/**
	 * Modo de Execu��o de RollBack.
	 */
	ROLLBACK
}
