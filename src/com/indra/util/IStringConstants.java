package com.indra.util;

/**
 * Constantes de String utilizadas na Aplica��o.
 * 
 * @author rmarini
 * 
 */
public interface IStringConstants {

	/**
	 * Nome do Arquivo de Propriedade que cont�m as configura��es Gerais da
	 * Aplica��o
	 */
	public static final String SQL_EXECUTOR_PROPERTIES_FILE = "SQLExecutor.properties";

	/**
	 * Nome do Arquivo de Propriedade com os dados de Conex�o com o Banco de
	 * Dados.
	 */
	public static final String ENVIRONMENT_PROPERTIES_FILE = "Environment.properties";

	/**
	 * Tag que indica a execu��o em ambiente de Desenvolvimento
	 */
	public static String DEV_ENVIRONMENT = "DEV";

	/**
	 * Prefixo de nome de arquivo que cont�m a lista ordenada de Scripts para
	 * Execu��o.
	 */
	public static String EXECUTION_FILE_PREFIX = "EXECUTE";

	/**
	 * Extens�o de Arquivos contendo Comandos SQL.
	 */
	public static String SQL_FILE_NAME = ".SQL";

	/**
	 * Tag para identifica��o de defini��o de Vers�o.
	 */
	public static String INGRID_VERSION_TAG = "INGRID_VERSION=";

	/**
	 * Tag para identifica��o de defini��o de Conex�o com o Banco de Dados.
	 */
	public static String SCHEMA_CONN_TAG = "CONN";

	/**
	 * Tag para identifica��o de Companhia (Companhias) ou Grupo (Grupos) onde o
	 * Script deve ser executado.
	 */
	public static final String COMPANY_WHERE_CAN_APPLY = "COMPANY_WHERE_CAN_APPLY";

	/**
	 * Strigs de Identifica��o de blocos contendo Comandos SQL.
	 */
	public static final String[] PLSQL_BLOCK_PREFIX = { "DECLARE\n", "BEGIN\n",
			"CREATE OR REPLACE PACKAGE", "CREATE OR REPLACE PROCEDURE",
			"CREATE OR REPLACE FUNCTION" };

}
