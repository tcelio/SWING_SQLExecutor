package com.indra.util.swing;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.indra.bean.AppliedVersion;
import com.indra.bean.SqlScriptVersion;
import com.indra.db.connection.DBConnectionManager;
import com.indra.util.Environment;
import com.indra.util.ExecutionMode;

public class Swing {

	private static final Log log = LogFactory.getLog(Swing.class);
   
	public static DBConnectionManager dbConnectionManager = null;

	// swing
	public static JFrame f = new JFrame("INGRID SQLExecutor");
	public static JMenuBar mb = new JMenuBar(); 
	public static JButton close_button = new JButton("Rollback");
	public static JButton patch_button = new JButton("Patch");

	public static JMenu file = new JMenu("File");
	public static JMenu help = new JMenu("Help");
	
	public static JMenu view = new JMenu("View");
	public static JMenuItem subItem_view_getAllVersion_applyed = new JMenuItem("Version history");
	public static JMenuItem subItem_view_getAllSqlScript_applyed = new JMenuItem("Sql Script history");
	
	
	public static JMenuItem subItem_databaseVersion = new JMenuItem("Data Base Version");
	public static JMenuItem subItem_databaseVersion_rollback = new JMenuItem("Apply Rollback");
	public static JMenuItem subItem_databaseVersion_patch = new JMenuItem("Apply Patch");
	//public static JMenuItem subItem_help_help = new JMenuItem(new ShowWaitAction("Show Wait Dialog"));
	public static JMenuItem subItem_help_about = new JMenuItem("About");

	public static JPanel panel1 = new JPanel();
	public static JLabel label1 = new JLabel("", JLabel.CENTER);
	public static JLabel label2 = new JLabel("", JLabel.CENTER);

	public Swing() {
		mb.setBounds(0, 0, 400, 20);
	}

	public static void start() {
		// cria o frame
		mostrarJanela();
		mostrarMenu();

		liberarMemoria();

		// showLoading();
		inserirIcone();

		// eventos
		helloWorld();
		pathButton_chooseDirectory();
		subItem_dataBaseVersion(null);
		subItem_helpAbout();

		ItemRollback();
		show();
		
		subItem_view_getAllVersion_applyed();
		subItem_view_getAllSqlScript_applyed();

	}

	public static void main(String[] args) {

		JButton showWaitBtn = new JButton(new ShowWaitAction("Show Wait Dialog"));
		JPanel panel = new JPanel();
		panel.add(showWaitBtn);
		JFrame frame = new JFrame("Frame");
		frame.getContentPane().add(panel);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);

	}

	public static void mostrarMenu() {

		file.add(subItem_databaseVersion);
		file.add(subItem_databaseVersion_patch);
		file.add(subItem_databaseVersion_rollback);

		view.add(subItem_view_getAllVersion_applyed);
		view.add(subItem_view_getAllSqlScript_applyed);
		
		
	//	help.add(subItem_help_help);
		help.add(subItem_help_about);

		f.setJMenuBar(mb);
		mb.add(file);
		mb.add(view);
		mb.add(help);
		// f.add(mb);
	}

	public static void mostrarJanela() {
		f.setSize(400, 250);
		f.setResizable(false);
		f.setLocationRelativeTo(null);
		f.setLayout(null);
		f.setVisible(true);
		panel1.setBounds(150, 60, 100, 100);
		panel1.add(label1, BorderLayout.CENTER);
		f.getContentPane().add(panel1);
		f.setVisible(true);

	}
	
	public static void inserirIcone() {
		try {
			f.setIconImage(ImageIO.read(new FileInputStream("images/icon.png")));
		} catch (Exception e) {

		}
	}

	public static void insertButtonClose() {

		close_button.setBounds(250, 100, 100, 30);
		f.add(close_button);
	}

	public static void helloWorld() {
		close_button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "License for utilities.", "INDRA - PRODUTO 2017", 2);

			}
		});
	}

	public static void liberarMemoria() {
		f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

	}

	public static void patchButton() {
		patch_button.setBounds(40, 100, 100, 30);
		f.add(patch_button);
	}

	public static void pathButton_chooseDirectory() {

		subItem_databaseVersion_patch.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				// show();
				label2.setText("Aguarde...");
				JFileChooser folderChooser = new JFileChooser();
				folderChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
				folderChooser.setDialogTitle("Escolha um diretorio.");
				folderChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				folderChooser.setAcceptAllFileFilterUsed(false);
				int returnValue = folderChooser.showOpenDialog(null);

				if (returnValue == JFileChooser.APPROVE_OPTION) {

					String path = folderChooser.getSelectedFile().toString();
					loadAndExecuteScripts(path, "PATCH");
					Environment.getInstance().setDevMode();
				} else if (returnValue == 1) {

					label2.setText("");

				}
			}
		});

	}

	public static void ItemRollback() {
		subItem_databaseVersion_rollback.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				label2.setText("Aguarde...");

				JFileChooser folderChooser = new JFileChooser();
				folderChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
				folderChooser.setDialogTitle("Escolha um diretorio.");
				folderChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				folderChooser.setAcceptAllFileFilterUsed(false);
				int returnValue = folderChooser.showOpenDialog(null);

				if (returnValue == JFileChooser.APPROVE_OPTION) {

					String path = folderChooser.getSelectedFile().toString();
					loadAndExecuteScripts(path, "ROLLBACK");
					Environment.getInstance().setExecutionMode(ExecutionMode.ROLLBACK);
				} else if (returnValue == 1) {
					label2.setText("");

				}

			}

		});
	}

	public static void loadAndExecuteScripts(String path, String type) {
		SwingEventsManager swingManager = new SwingEventsManager();
		try {

			swingManager.applyScripts(path, type);

		} catch (Exception e) {
			log.error("Error in the select path and execute script.");
		}

	}

	
	public static void subItem_view_getAllVersion_applyed(){
		

		
		subItem_view_getAllVersion_applyed.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

			    JPanel topPanel = new JPanel();
			    JTable table = new JTable();
			    JScrollPane scrollPane; // para rolar o painel
			    JFrame fTable = new JFrame("INGRID SQLExecutor");
				fTable.setTitle("All versions");
				fTable.setSize(800, 300);
				fTable.setBackground(Color.gray);
				
				SwingEventsManager svm = new SwingEventsManager();
				List<AppliedVersion> av = new ArrayList<AppliedVersion>();
				av = svm.getAllappliedVersion(dbConnectionManager);
						int i = 0;
				 String[][] o = new String[av.size()][6];
				for (AppliedVersion adv : av) {
				            
							o[i][0] = adv.getName();
				            o[i][1] = adv.getVersion();
				            o[i][2] = adv.getDate();
				            o[i][3] = adv.getUser();
				            o[i][4] = adv.getMachine();
				            o[i][5] = adv.getEvent();
				            i++;
				          
				        }
		
		        topPanel.setLayout(new BorderLayout());
		        fTable.getContentPane().add(topPanel);
		        String colunas[] = {"Name", "Version", "Date", "User", "Machine", "Event" };
		        table = new JTable(o, colunas);
		        scrollPane = new JScrollPane(table);
		        topPanel.add(scrollPane, BorderLayout.CENTER);
		        fTable.setLocationRelativeTo(null);
				fTable.setVisible(true);
				
			}
		});
	}
	
	

	
	
	public static void subItem_view_getAllSqlScript_applyed(){
		subItem_view_getAllSqlScript_applyed.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				 JPanel topPanel = new JPanel();
				    JTable table = new JTable();
				    JScrollPane scrollPane; // para rolar o painel
				    JFrame fTable = new JFrame("INGRID SQLExecutor");
					fTable.setTitle("SQL Scripts order desc");
					fTable.setSize(800, 300);
					fTable.setBackground(Color.gray);
					
					SwingEventsManager svm = new SwingEventsManager();
					List<SqlScriptVersion> av = new ArrayList<SqlScriptVersion>();
					av = svm.getAllSqlScript(dbConnectionManager);
							int i = 0;
					 String[][] o = new String[av.size()][5];
					for (SqlScriptVersion adv : av) {
								o[i][0] = adv.getScriptName();
					            o[i][1] = adv.getProject();
					            o[i][2] = adv.getEventStatus();
					            o[i][3] = adv.getVersion();
					            o[i][4] = adv.getDate();
					            i++;
					        }
			
			        topPanel.setLayout(new BorderLayout());
			        fTable.getContentPane().add(topPanel);
			        String colunas[] = {"Script Path", "Project", "Status", "Version", "Date"};
			        table = new JTable(o, colunas);
			        scrollPane = new JScrollPane(table);
			        topPanel.add(scrollPane, BorderLayout.CENTER);
			        fTable.setLocationRelativeTo(null);
					fTable.setVisible(true);
			}
		});
	}
	
	public static void subItem_dataBaseVersion(String version) {
		subItem_databaseVersion.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				SwingEventsManager svm = new SwingEventsManager();
				String version = svm.callAppVersion(dbConnectionManager);

				JOptionPane.showMessageDialog(null, "Vers�o: " + version + "", "INDRA - PRODUTO 2017", 2);
			}
		});

	}

	public static void subItem_helpAbout() {
		subItem_help_about.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null,
						"Esta aplica��o foi criada a fim \n de ajudar usuarios do InGRID \n a atualizarem"
								+ "o banco de dados \ndo sistema InGRID.\n",
						"Caracteristicas do SQLExecutor", 2);

			}
		});

	}

	public static void successExecution() {
		int choice = JOptionPane.showConfirmDialog(null, "Success", "Result", JOptionPane.OK_CANCEL_OPTION);
		if (choice == JOptionPane.OK_OPTION || choice == JOptionPane.CANCEL_OPTION) {
			label2.setText("");
		}
	}

	public static void show() {

		panel1.add(label2, BorderLayout.CENTER);
		f.getContentPane().add(panel1);
		f.setVisible(true);
	}

	public static void showError(Exception ex, String logStr) {
		label2.setText("");
		JOptionPane.showMessageDialog(null, logStr, "Erro!", JOptionPane.ERROR_MESSAGE);
	}

	public static void showErrorStr(String logStr) {
		label2.setText("");
		JOptionPane.showMessageDialog(null, logStr, "Erro!", JOptionPane.ERROR_MESSAGE);
		
	}


	
	

}
