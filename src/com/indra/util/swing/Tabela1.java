package com.indra.util.swing;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

class Tabela1 extends JFrame {
    // Atributos
    private JPanel topPanel;
    private JTable table;
    private JScrollPane scrollPane; // para rolar o painel

    public Tabela1() {
        // configura��o do JFrame
        setTitle("Uma Tabela Simples");
        setSize(300, 200);
        setBackground(Color.gray);

        // Cria o painel
        topPanel = new JPanel();
        topPanel.setLayout(new BorderLayout());
        getContentPane().add(topPanel);

        // Cria os nomes das colunas
        String colunas[] = { "M�veis", "Eletro", "Utens�lios" };

        // Cria os dados
        String valores[][] = { { "Sof�", "TV", "Faca" },
                { "Cama", "Aspirador", "Colher" },
                { "Mesa", "Lavadora", "Garfo" },
                { "Cadeira", "Liquidificador", "Copo" },
                { "Cadeira", "Liquidificador", "Copo" },
                { "Cadeira", "Liquidificador", "Copo" },
                { "Cadeira", "Liquidificador", "Copo" },
                { "Cadeira", "Liquidificador", "Copo" },
                { "Cadeira", "Liquidificador", "Copo" },
                { "Cadeira", "Liquidificador", "Copo" },
                { "Cadeira", "Liquidificador", "Copo" },
                { "Cadeira", "Liquidificador", "Copo" },
                { "Cadeira", "Liquidificador", "Copo" },
                { "Cadeira", "Liquidificador", "Copo" },
                { "Cadeira", "Liquidificador", "Copo" },
                { "Cadeira", "Liquidificador", "Copo" }
                
        };

        // Cria o JTable
        table = new JTable(valores, colunas);

        // Confirua  o JTable com JScrollPane e este �ltino � adicionado dento do JPanel
        scrollPane = new JScrollPane(table);
        topPanel.add(scrollPane, BorderLayout.CENTER);
    }

    public static void main(String args[]) {
        // Executa o JFrame
        Tabela1 vai = new Tabela1();
        vai.setVisible(true);
    }
}