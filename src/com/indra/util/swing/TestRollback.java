package com.indra.util.swing;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class TestRollback {
	
	private static final Log log = LogFactory.getLog(TestRollback.class);
	private static Map<String, String> rblist = new HashMap<String, String>();
	private static List<EventBean> eventlist = new ArrayList<EventBean>();
	private static EventBean lastEvent = new EventBean();

	public static void main(String[] args) {
			
		Collections.reverse(eventlist);
		boolean validator = false;
		for(EventBean b : eventlist){

			if(b.getType().matches("ROLLBACK")){
				addRollbackValue(b.getType(), b.getVersion());
				log.info("inserido no rollback -> "+b.getVersion());
			}
			if(b.getType().matches("PATCH") && !containVersion(b.getVersion())){
				if(!validator){
					lastEvent.setId(b.getId());
					lastEvent.setVersion(b.getVersion());
					lastEvent.setType("PATCH");
					log.info("inserido no last eventBean-> "+b.getVersion());
					validator = true;
				}
			}
		}
		mostrarVersao();
		listRollbacks();
	}

	public static void mostrarVersao(){
		System.out.println("versao atual eh: "+lastEvent.getVersion()+" ID: "+lastEvent.getId());
	}
	
	public static void addRollbackValue(String type, String version){
		rblist.put(version, type);
	}
	
	public static void addEventValue(EventBean event){
		eventlist.add(event);
	}
	
	public static boolean containVersion(String version){
		return rblist.containsKey(version);
	}
		
	public static void listRollbacks(){
		
		try {
			Iterator iterator = rblist.entrySet().iterator();
			
			while(iterator.hasNext()){
				Map.Entry map = (Map.Entry) iterator.next();
				System.out.println("TYPE: "+map.getKey());
				System.out.println("VERSION: "+map.getValue());
			}
			
		} catch (Exception e) {
			System.out.println("Error: "+e.getCause());
		}
		
	}

	public static void listEvents(){
		
		try {
			for(EventBean r : eventlist){
				System.out.println("ID: "+r.getId());
				System.out.println("TYPE: "+r.getType());
				System.out.println("VERSION: "+r.getVersion());
			}
		} catch (Exception e) {
			System.out.println("Error:: "+e.getCause());
		}
		
	}
	
	
	static{

/*		RollbackBean r1 = new RollbackBean();
		r1.setId(1);
		r1.setVersion("1.0");
				
		RollbackBean r2 = new RollbackBean();
		r2.setId(2);
		r2.setVersion("1.2");
		
		RollbackBean r3 = new RollbackBean();
		r3.setId(3);
		r3.setVersion("1.3");
		
		RollbackBean r4 = new RollbackBean();
		r4.setId(4);
		r4.setVersion("2.0");
		
		RollbackBean r5 = new RollbackBean();
		r5.setId(5);
		r5.setVersion("2.1");
		
		RollbackBean r6 = new RollbackBean();
		r6.setId(6);
		r6.setVersion("2.2");
		
		RollbackBean r7 = new RollbackBean();
		r7.setId(7);
		r7.setVersion("2.3");
	
		addRollbackValue(r1);addRollbackValue(r2);addRollbackValue(r3);
		addRollbackValue(r4);addRollbackValue(r5);addRollbackValue(r6);
		addRollbackValue(r7);
*/		
		//CEN�RIO 1:
		
/*		EventBean e1 = new EventBean();
		e1.setId(1);
		e1.setType("PATCH");
		e1.setVersion("1.0");
		
		EventBean e2 = new EventBean();
		e2.setId(2);
		e2.setType("PATCH");
		e2.setVersion("1.1");
		
		EventBean e3 = new EventBean();
		e3.setId(3);
		e3.setType("PATCH");
		e3.setVersion("1.2");
		
		EventBean e4 = new EventBean();
		e4.setId(4);
		e4.setType("PATCH");
		e4.setVersion("1.3");
		
		EventBean e5 = new EventBean();
		e5.setId(5);
		e5.setType("ROLLBACK");
		e5.setVersion("1.3");
		
		EventBean e6 = new EventBean();
		e6.setId(6);
		e6.setType("ROLLBACK");
		e6.setVersion("1.2");
		
		EventBean e7 = new EventBean();
		e7.setId(7);
		e7.setType("ROLLBACK");
		e7.setVersion("1.1");
		
		addEventValue(e1);addEventValue(e2);addEventValue(e3);
		addEventValue(e4);addEventValue(e5);addEventValue(e6);
		addEventValue(e7);*/
		
		//CEN�RIO 2:
		
/*		EventBean e1 = new EventBean();
		e1.setId(1);
		e1.setType("PATCH");
		e1.setVersion("1.0");
		
		EventBean e2 = new EventBean();
		e2.setId(2);
		e2.setType("PATCH");
		e2.setVersion("1.1");
		
		EventBean e3 = new EventBean();
		e3.setId(3);
		e3.setType("PATCH");
		e3.setVersion("1.2");
		
		EventBean e4 = new EventBean();
		e4.setId(4);
		e4.setType("PATCH");
		e4.setVersion("1.3");
		
		EventBean e5 = new EventBean();
		e5.setId(5);
		e5.setType("ROLLBACK");
		e5.setVersion("1.3");
		
		EventBean e6 = new EventBean();
		e6.setId(6);
		e6.setType("ROLLBACK");
		e6.setVersion("1.2");
		
		EventBean e7 = new EventBean();
		e7.setId(7);
		e7.setType("ROLLBACK");
		e7.setVersion("1.1");
		
		EventBean e8 = new EventBean();
		e8.setId(8);
		e8.setType("PATCH");
		e8.setVersion("1.1");
		
		EventBean e9 = new EventBean();
		e9.setId(9);
		e9.setType("PATCH");
		e9.setVersion("1.2");
		
		EventBean e10 = new EventBean();
		e10.setId(10);
		e10.setType("PATCH");
		e10.setVersion("1.3");
		
		EventBean e11 = new EventBean();
		e11.setId(11);
		e11.setType("ROLLBACK");
		e11.setVersion("1.3");
		
		EventBean e12 = new EventBean();
		e12.setId(12);
		e12.setType("ROLLBACK");
		e12.setVersion("1.2");
		
		EventBean e13 = new EventBean();
		e13.setId(13);
		e13.setType("ROLLBACK");
		e13.setVersion("1.1");
		
		
		addEventValue(e1);addEventValue(e2);addEventValue(e3);
		addEventValue(e4);addEventValue(e5);addEventValue(e6);
		addEventValue(e7);addEventValue(e8);addEventValue(e9);
		addEventValue(e10);addEventValue(e11);addEventValue(e12);
		addEventValue(e13);*/
		
		/*//CEN�RIO 3 :
		
		EventBean e1 = new EventBean();
		e1.setId(1);
		e1.setType("PATCH");
		e1.setVersion("1.0");
		
		EventBean e2 = new EventBean();
		e2.setId(2);
		e2.setType("PATCH");
		e2.setVersion("1.1");
		
		EventBean e3 = new EventBean();
		e3.setId(3);
		e3.setType("PATCH");
		e3.setVersion("1.2");
		
		EventBean e4 = new EventBean();
		e4.setId(4);
		e4.setType("PATCH");
		e4.setVersion("1.3");
		
		EventBean e5 = new EventBean();
		e5.setId(5);
		e5.setType("ROLLBACK");
		e5.setVersion("1.3");
		
		EventBean e6 = new EventBean();
		e6.setId(6);
		e6.setType("ROLLBACK");
		e6.setVersion("1.2");
		
		EventBean e7 = new EventBean();
		e7.setId(7);
		e7.setType("ROLLBACK");
		e7.setVersion("1.1");
		
		EventBean e8 = new EventBean();
		e8.setId(8);
		e8.setType("PATCH");
		e8.setVersion("1.1");
		
		EventBean e9 = new EventBean();
		e9.setId(9);
		e9.setType("PATCH");
		e9.setVersion("1.2");
		
		EventBean e10 = new EventBean();
		e10.setId(10);
		e10.setType("PATCH");
		e10.setVersion("1.3");
		
		EventBean e11 = new EventBean();
		e11.setId(11);
		e11.setType("ROLLBACK");
		e11.setVersion("1.3");
		
		EventBean e12 = new EventBean();
		e12.setId(12);
		e12.setType("ROLLBACK");
		e12.setVersion("1.2");
		
		EventBean e13 = new EventBean();
		e13.setId(13);
		e13.setType("ROLLBACK");
		e13.setVersion("1.1");
		
		EventBean e14 = new EventBean();
		e14.setId(14);
		e14.setType("PATCH");
		e14.setVersion("2.0");
		
		addEventValue(e1);addEventValue(e2);addEventValue(e3);
		addEventValue(e4);addEventValue(e5);addEventValue(e6);
		addEventValue(e7);addEventValue(e8);addEventValue(e9);
		addEventValue(e10);addEventValue(e11);addEventValue(e12);
		addEventValue(e13);addEventValue(e14);*/
	
		
		//CEN�RIO 4 :
		
				EventBean e1 = new EventBean();
				e1.setId(1);
				e1.setType("PATCH");
				e1.setVersion("1.0");
				
				EventBean e2 = new EventBean();
				e2.setId(2);
				e2.setType("PATCH");
				e2.setVersion("1.1");
				
				EventBean e3 = new EventBean();
				e3.setId(3);
				e3.setType("PATCH");
				e3.setVersion("1.2");
				
				EventBean e4 = new EventBean();
				e4.setId(4);
				e4.setType("PATCH");
				e4.setVersion("1.3");
				
				EventBean e5 = new EventBean();
				e5.setId(5);
				e5.setType("ROLLBACK");
				e5.setVersion("1.3");
				
				EventBean e6 = new EventBean();
				e6.setId(6);
				e6.setType("ROLLBACK");
				e6.setVersion("1.2");
				
				EventBean e7 = new EventBean();
				e7.setId(7);
				e7.setType("ROLLBACK");
				e7.setVersion("1.1");
				
				EventBean e8 = new EventBean();
				e8.setId(8);
				e8.setType("PATCH");
				e8.setVersion("1.1");
				
				EventBean e9 = new EventBean();
				e9.setId(9);
				e9.setType("PATCH");
				e9.setVersion("1.2");
				
				EventBean e10 = new EventBean();
				e10.setId(10);
				e10.setType("PATCH");
				e10.setVersion("1.3");
				
				EventBean e11 = new EventBean();
				e11.setId(11);
				e11.setType("ROLLBACK");
				e11.setVersion("1.3");
				
				EventBean e12 = new EventBean();
				e12.setId(12);
				e12.setType("ROLLBACK");
				e12.setVersion("1.2");
				
				EventBean e13 = new EventBean();
				e13.setId(13);
				e13.setType("ROLLBACK");
				e13.setVersion("1.1");
				
				EventBean e14 = new EventBean();
				e14.setId(14);
				e14.setType("PATCH");
				e14.setVersion("2.0");
				
				EventBean e15 = new EventBean();
				e15.setId(15);
				e15.setType("PATCH");
				e15.setVersion("2.1");
				
				EventBean e16 = new EventBean();
				e16.setId(16);
				e16.setType("PATCH");
				e16.setVersion("2.2");
				
				EventBean e17 = new EventBean();
				e17.setId(17);
				e17.setType("PATCH");
				e17.setVersion("2.3");
				
				EventBean e18 = new EventBean();
				e18.setId(18);
				e18.setType("ROLLBACK");
				e18.setVersion("2.3");
				
				EventBean e19 = new EventBean();
				e19.setId(19);
				e19.setType("ROLLBACK");
				e19.setVersion("2.2");
				
				EventBean e20 = new EventBean();
				e20.setId(20);
				e20.setType("ROLLBACK");
				e20.setVersion("2.1");
				
				EventBean e21 = new EventBean();
				e21.setId(21);
				e21.setType("ROLLBACK");
				e21.setVersion("2.0");
				
				addEventValue(e1);addEventValue(e2);addEventValue(e3);
				addEventValue(e4);addEventValue(e5);addEventValue(e6);
				addEventValue(e7);addEventValue(e8);addEventValue(e9);
				addEventValue(e10);addEventValue(e11);addEventValue(e12);
				addEventValue(e13);addEventValue(e14);addEventValue(e15);
				addEventValue(e16);addEventValue(e17);addEventValue(e18);
				addEventValue(e19);addEventValue(e20);addEventValue(e21);
		
	}

}
