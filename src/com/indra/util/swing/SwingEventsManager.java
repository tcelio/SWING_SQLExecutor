package com.indra.util.swing;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.indra.bean.AppliedVersion;
import com.indra.bean.SqlScriptVersion;
import com.indra.db.DBConnection;
import com.indra.db.connection.DBConnectionManager;
import com.indra.db.connection.DBConnectionParser;
import com.indra.executor.SQLExecutorManager;
import com.indra.executor.register.RegisterSQLExecution;
import com.indra.sql.ProductVersion;
import com.indra.sql.parser.VersionParser;
import com.indra.util.Environment;
import com.indra.util.ExecutionMode;
import com.indra.util.IStringConstants;
import com.indra.util.PropertyUtil;

public class SwingEventsManager {

	private static final Log log = LogFactory.getLog(SwingEventsManager.class);
	private static String EXECUTION_FILE_OR_DIR = "";
	private static Boolean loadDataBase = true;
	private static Boolean executeScripts = true;

	private static String appName = "";
	private static String companyName = "";
	private static String companyGroup = "";
	private static Boolean stopOnError = Boolean.FALSE;
	private static String connectionApplyPatchLog = "";
	
	public SwingEventsManager(){
		
	}
	
	public String callAppVersion(DBConnectionManager dbConnectionManager){

		DBConnectionManager connection = new DBConnectionManager();
		DBConnection dbConnection = null;
		dbConnection = connection.createDBConnection("INGRID", "jdbc:oracle:thin:@192.200.9.26:1521:zeus/ELEKTRO:brutal");
		String v = RegisterSQLExecution.getCurrentVersion(dbConnection, 1);
		return v;

	}
	
	public List<AppliedVersion> getAllappliedVersion(DBConnectionManager dbConnectionManager){
		DBConnectionManager connection = new DBConnectionManager();
		DBConnection dbConnection = null;
		List<AppliedVersion> list = new ArrayList<AppliedVersion>();
		dbConnection = connection.createDBConnection("INGRID", "jdbc:oracle:thin:@192.200.9.26:1521:zeus/ELEKTRO:brutal");
		list = RegisterSQLExecution.getAllAppliedVersion(dbConnection, 1);
		return list;
	}
	
	
	public List<SqlScriptVersion> getAllSqlScript(DBConnectionManager dbConnectionManager){
		DBConnectionManager connection = new DBConnectionManager();
		DBConnection dbConnection = null;
		List<SqlScriptVersion> list = new ArrayList<SqlScriptVersion>();
		dbConnection = connection.createDBConnection("INGRID", "jdbc:oracle:thin:@192.200.9.26:1521:zeus/ELEKTRO:brutal");
		list = RegisterSQLExecution.getAllScriptExecuted(dbConnection, 1);
		return list;
	}
	public void applyScripts(String path, String rollback) {
		try {
			if (ExecutionMode.ROLLBACK.name().equals(rollback)) {
				Environment.getInstance().setExecutionMode(
						ExecutionMode.ROLLBACK);
				System.out.println(Environment.getInstance().getExecutionMode()+"==");
				if (carregaConfiguracoesIniciais()) {
					DBConnectionManager connection = setConnections();
					SQLExecutorManager sqlExecutorManager = getConnection(connection);
					ArrayList<String> pathList = sqlExecutorManager.getLastPatch(connection);
					VersionParser versionParserRoolback = new VersionParser(1);
					String version = versionParserRoolback.loadVersionRollback(new File(path), pathList);
					ArrayList<ProductVersion> versionsList1 = versionParserRoolback.loadVersion(new File(version), 1);
					if (!versionsList1.isEmpty()) {
						if (sqlExecutorManager != null) {
							System.out.println("VAlue"+Environment.getInstance().getExecutionMode().toString());
							executeScript(sqlExecutorManager, Environment.getInstance().getExecutionMode(),
									versionsList1, connection);
						}
					}
				}
				
			} else if (ExecutionMode.PATCH.name().equals(rollback)) {
				log.warn("Definida Execu��o em Ambiente de Desenvolvimento!");
				Environment.getInstance().setDevMode();

				// carrega configuracoes
				if (carregaConfiguracoesIniciais()) {
					ArrayList<ProductVersion> versionsList = carregaScripts(path);
					if (!versionsList.isEmpty()) {
						DBConnectionManager connection = setConnections();
						SQLExecutorManager sqlExecutorManager = validateConnections(connection, versionsList);
						if (sqlExecutorManager != null) {
							executeScript(sqlExecutorManager, Environment.getInstance().getExecutionMode(),
									versionsList, connection);
						}
					}
				}
			}

		} catch (Exception e) {
			System.out.println("Error: " + e.getCause());
			showException(e, "Error in the execution method in SwingEventsManager.");
			log.error("Error in the execution method in SwingEventsManager.");
			
		}

	}	
	
	public static boolean setProductVersionRollBack(ArrayList<ProductVersion> versionsList){
		boolean flag = false;
		for (ProductVersion productVersion : versionsList) {
			productVersion.setExecutionMode(ExecutionMode.ROLLBACK);
			flag = true;
		}
		
		return flag;
	}
	
	private void executeScript(SQLExecutorManager sqlExecutorManager, ExecutionMode executionMode, ArrayList<ProductVersion> versionsList,
			DBConnectionManager connection) {
		
		
		log.info("******************************************");
		log.info("* Executando os Scripts na base de dados..");
		log.info("******************************************");
		
	
			sqlExecutorManager.applyScripts(Environment.getInstance()
					.getExecutionMode(), versionsList, connection, 1);
			sucesso();
			log.info("********************************");
			log.info("* Finalizada aplica��o da Vers�o");
			log.info("********************************");
	}

	/**
	 * 
	 * @return
	 */
	public static boolean carregaConfiguracoesIniciais(){
		boolean flag = false;
		try {
			String config = System.getProperty("config.dir");
			String urlAux = IStringConstants.SQL_EXECUTOR_PROPERTIES_FILE;
			if (config != null) {
				urlAux = config + "/"
						+ IStringConstants.SQL_EXECUTOR_PROPERTIES_FILE;
			}
			Properties properties = new PropertyUtil().getProperties(urlAux);
			if (properties != null) {
				appName = properties.getProperty("AppName", "");
				companyName = properties.getProperty("CompanyName", "");
				companyGroup = properties.getProperty("CompanyGroup", "");
				stopOnError = Boolean.valueOf(properties.getProperty(
						"StopOnError", "TRUE"));
				connectionApplyPatchLog = properties.getProperty(
						"ConnecToRegisterApplyPatchLog", "");
				flag = true;
			}
		} catch (IOException e) {
			showException(e, "Erro ao ler o arquivo de configura��o "
					+ IStringConstants.ENVIRONMENT_PROPERTIES_FILE + ": " + e);
			log.error("Erro ao ler o arquivo de configura��o "
					+ IStringConstants.ENVIRONMENT_PROPERTIES_FILE + ": " + e);
			
		}
		return flag;
	}
	
	
	public static ArrayList<ProductVersion> carregaScriptsRollback(String executionFileOrDir){
		log.info("*************************");
		log.info("* Carregando os Scripts..");
		log.info("*************************");
		ArrayList<ProductVersion> versionsList = null;
		try {
		
			VersionParser versionParser = new VersionParser();
			versionsList = versionParser
					.loadVersion(new File(executionFileOrDir), 1);	
			
				if (versionsList == null) {
					
					log.error("N�o foi carregada nenhuma vers�o do caminho encontrado: "
							+ executionFileOrDir);
				} else {
					for (ProductVersion productVersion : versionsList) {
						if (productVersion.hasProblem()) {
							log.fatal("Encontrados problemas nos Scripts da Vers�o: "
									+ productVersion.getVersion());
							productVersion.printProblems(log);
						} else {
							productVersion.setExecutionMode(ExecutionMode.PATCH);
							log.info("N�o foi encontrado nenhum problema nos Scripts da Vers�o: "
									+ productVersion.getVersion());
						}
					}
				}
			
		} catch (Exception e) {
			showException(e, "Falha ao carregar os scripts de vers�o do rollback.\n"
					+ "Verificar se os mesmos equivalem aos scripts do arquivo 'Execute'.");
			log.error("Falha ao carregar os scripts de vers�o.\nVerificar se os mesmos equivalem aos scripts do arquivo 'Execute'.");
		}
		
			return versionsList;
	}
	
	
	
	/**
	 * 
	 * @param executionFileOrDir
	 * @return
	 */
	public static ArrayList<ProductVersion> carregaScripts(String executionFileOrDir){
		
		log.info("*************************");
		log.info("* Carregando os Scripts..");
		log.info("*************************");
		ArrayList<ProductVersion> versionsList = null;
		try {
			VersionParser versionParser = new VersionParser();
			versionsList = versionParser
					.loadVersion(new File(executionFileOrDir), 1);	
			
				if (versionsList == null) {
					
					log.error("N�o foi carregada nenhuma vers�o do caminho encontrado: "
							+ executionFileOrDir);
				} else {
					for (ProductVersion productVersion : versionsList) {
						if (productVersion.hasProblem()) {
							log.fatal("Encontrados problemas nos Scripts da Vers�o: "
									+ productVersion.getVersion());
							productVersion.printProblems(log);
						} else {
							productVersion.setExecutionMode(ExecutionMode.PATCH);
							log.info("N�o foi encontrado nenhum problema nos Scripts da Vers�o: "
									+ productVersion.getVersion());
						}
					}
				}
		} catch (Exception e) {
			showException(e, "Falha ao carregar os scripts de vers�o do Patch.\n"
					+ "Verificar se os mesmos equivalem aos scripts do arquivo 'Execute'.");
			log.error("Falha ao carregar os scripts de vers�o.\nVerificar se os mesmos equivalem aos scripts do arquivo 'Execute'.");
		}
		
			return versionsList;
	}
	
	
	public static DBConnectionManager setConnections(){
		/***************************************
		 * FASE 3 - Conecta na Base de Dados
		 ***************************************/
		log.info("************************");
		log.info("* Carregando Conex�es..*");
		log.info("************************");
		DBConnectionManager dbConnectionManager = null;

		try {
			DBConnectionParser connectionParser = new DBConnectionParser();
			dbConnectionManager = connectionParser.getDBConnectionManager();
			if (dbConnectionManager.hasConnectionError()) {
				log.warn("N�o foi poss�vel conectar em todos "
						+ "os schemas configurados " + "no arquivo "
						+ IStringConstants.ENVIRONMENT_PROPERTIES_FILE);
				dbConnectionManager.printDBConnectionError();
			} else {
				log.info("Conex�o com a Base de Dados bem sussedida.");
			}
		} catch (Exception e) {
			showException(e, "Falha ao conectar �s bases de dados. \nVerificar as informa��es de conex�o!");
			log.error("Falha ao conectar �s bases de dados. \nVerificar as informa��es de conex�o!");
		}
			
			return dbConnectionManager;
	}
	
	
	public static SQLExecutorManager validateConnections(DBConnectionManager dbConnectionManager,
			ArrayList<ProductVersion> versionsList) {

		log.info("**********************");
		log.info("* Validando Conex�es..");
		log.info("**********************");
		SQLExecutorManager sqlExecutorManager = new SQLExecutorManager(appName, companyName, companyGroup);

		if (dbConnectionManager != null) {
			sqlExecutorManager.stopOnError(stopOnError);
			sqlExecutorManager.setConnectionApplyPatchLog(dbConnectionManager.getConnection(connectionApplyPatchLog));

			String requiredConnection = "";

			requiredConnection = sqlExecutorManager.getDisconnectedRequiredDataSources(versionsList,
					dbConnectionManager);
			if (requiredConnection == null || requiredConnection.length() == 0) {
				log.info("Cont�m todas as conex�es necess�rias");
			} else {
				log.fatal("N�o cont�m todas as conex�es necess�rias: " + requiredConnection);
			}

		}
		return sqlExecutorManager;

	}
	
	public static SQLExecutorManager getConnection(DBConnectionManager dbConnectionManager){
		log.info("**********************");
		log.info("* Validando Conex�es..");
		log.info("**********************");
		
		SQLExecutorManager sqlExecutorManager = null;
		try {
			sqlExecutorManager = new SQLExecutorManager(appName, companyName, companyGroup);

			if (dbConnectionManager != null) {
				sqlExecutorManager.stopOnError(stopOnError);
				sqlExecutorManager.setConnectionApplyPatchLog(dbConnectionManager.getConnection(connectionApplyPatchLog));

			}
		} catch (Exception e) {
			showException(e,"Falha ao obter a conex�o com a base de dados.");
		}
		
		return sqlExecutorManager;
	}
	
	
	public void sucesso(){
		Swing.successExecution();
	}

	public static void showException(Exception e, String logStr){
		
		Swing.showError(e, logStr);
		
	}

	public static void showExceptionStr(String logStr){
		
		Swing.showErrorStr(logStr);
		
	}

}
