package com.indra.util;

public class Environment {

	private static Environment instance = null;

	private Boolean isDevMode = Boolean.FALSE;

	private ExecutionMode executionMode = ExecutionMode.PATCH;

	protected Environment() {
	}

	/**
	 * Obt�m a instancia do Objeto <code>Environment</code>.
	 * 
	 * @return Instancia do objeto Environment.
	 */
	public static Environment getInstance() {
		if (instance == null) {
			instance = new Environment();
		}
		return instance;
	}

	/**
	 * Seta o Ambiente como execu��o em Ambiente de Desenvolvimento.
	 */
	public void setDevMode() {
		isDevMode = Boolean.TRUE;
	}

	/**
	 * Seta o Modo de Execu��o dos Scripts.
	 * 
	 * @param execMode
	 *            Modo de Execu��o do Script.
	 */
	public void setExecutionMode(ExecutionMode execMode) {
		this.executionMode = execMode;
	}

	/**
	 * Verifica se os scripts ser�o executados em ambiente de desenvolvimento.
	 * 
	 * @return Se � <code>true</code> ou n�o <code>false</code> execu��o em
	 *         ambiente de Desenvolvimento.
	 */
	public Boolean isDevMode() {
		return isDevMode;
	}

	/**
	 * Obt�m o modo de Execu��o dos Scripts.
	 * 
	 * @return Modo de Execu��o dos Scripts.
	 */
	public ExecutionMode getExecutionMode() {
		return executionMode;
	}

}
