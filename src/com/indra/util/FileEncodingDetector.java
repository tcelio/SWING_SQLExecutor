package com.indra.util;

import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.mozilla.universalchardet.UniversalDetector;

/**
 * Classe respons�vel por detectar o Encoding utilizado no Arquivo, para que os
 * caracteres especiais n�o sejam removidos durante a aplica��o dos Scripts na
 * Base de Dados.
 * 
 * @author rmarini
 * 
 */
public class FileEncodingDetector {

	public static final String DEFAULT_ENCODING = "UTF-8";

	/**
	 * Detecta o Encoding utilizado no Arquivo.
	 * 
	 * @param file
	 *            Arquivo a ser verificado
	 * @return Encoding identificado no Arquivo
	 * @throws IOException
	 *             Exce��o de acesso ao Arquivo
	 */
	public static String detectEncoding(File file) throws IOException {
		byte[] buf = new byte[4096];

		FileInputStream fis = new FileInputStream(file);
		UniversalDetector detector = new UniversalDetector(null);

		int nread;
		while ((nread = fis.read(buf)) > 0 && !detector.isDone())
			detector.handleData(buf, 0, nread);

		closeStream(fis);

		detector.dataEnd();
		String encoding = detector.getDetectedCharset();

		if (encoding == null)
			encoding = DEFAULT_ENCODING;
		return encoding;
	}

	/**
	 * Fecha o Arquivo aberto ap�s sua leitura.
	 * 
	 * @param stream
	 *            Objeto <code>FileInputStream</code> a ser fechado
	 */
	private static void closeStream(Closeable stream) {
		if (stream != null) {
			try {
				stream.close();
				stream = null;
			} catch (IOException e) {
				// N�o precisa fazer nada
			}
		}
	}

}
