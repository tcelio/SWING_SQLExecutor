package com.indra.rollback;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.indra.util.swing.EventBean;

public class RollbackAnalise {

	
	private static final Log log = LogFactory.getLog(RollbackAnalise.class);
	private static Map<String, String> rblist = new HashMap<String, String>();
	private static List<EventBean> eventlist = new ArrayList<EventBean>();
	private static EventBean lastEvent = new EventBean();
	
	public RollbackAnalise(){
		
		
	}
	
	//pegar a lista de evento de rollbacl e apply de patch da tabela "ZEUS_VERSION_APPLIED"
	
	
	//pega a versao atual da aplicacao registrada no banco de dados da tabela "ZEUS_VERSION_APP"
	
	public static void mostrarVersao(){
		System.out.println("versao atual eh: "+lastEvent.getVersion()+" ID: "+lastEvent.getId());
	}
	
	public static void addRollbackValue(String type, String version){
		rblist.put(version, type);
	}
	
	public static void addEventValue(EventBean event){
		eventlist.add(event);
	}
	
	public static boolean containVersion(String version){
		return rblist.containsKey(version);
	}
		
	public static void listRollbacks(){
		
		try {
			Iterator iterator = rblist.entrySet().iterator();
			
			while(iterator.hasNext()){
				Map.Entry map = (Map.Entry) iterator.next();
				System.out.println("TYPE: "+map.getKey());
				System.out.println("VERSION: "+map.getValue());
			}
			
		} catch (Exception e) {
			System.out.println("Error: "+e.getCause());
		}
		
	}

	public static void listEvents(){
		
		try {
			for(EventBean r : eventlist){
				System.out.println("ID: "+r.getId());
				System.out.println("TYPE: "+r.getType());
				System.out.println("VERSION: "+r.getVersion());
			}
		} catch (Exception e) {
			System.out.println("Error:: "+e.getCause());
		}
		
	}
}
