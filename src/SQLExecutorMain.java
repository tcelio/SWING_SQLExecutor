import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.indra.db.connection.DBConnectionManager;
import com.indra.db.connection.DBConnectionParser;
import com.indra.executor.SQLExecutorManager;
import com.indra.sql.ProductVersion;
import com.indra.sql.parser.VersionParser;
import com.indra.util.Environment;
import com.indra.util.ExecutionMode;
import com.indra.util.IStringConstants;
import com.indra.util.PropertyUtil;
import com.indra.util.swing.Swing;

public class SQLExecutorMain {

	private static final Log log = LogFactory.getLog(SQLExecutorMain.class);

	//private static String EXECUTION_FILE_OR_DIR = "E:\\TMP\\Scripts_Ingrid_3.3.0.14\\execute.txt";
	private static String EXECUTION_FILE_OR_DIR = "";
	private static Boolean loadDataBase = true;
	private static Boolean executeScripts = true;

	private static String appName = "";
	private static String companyName = "";
	private static String companyGroup = "";
	private static Boolean stopOnError = Boolean.FALSE;
	private static String connectionApplyPatchLog = "";

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		 if(args.length == 0){
			 Swing.start();
				}else{
					start(args);
				}
		
	}

	public static void start(String[] args){
	
	String executionFileOrDir = EXECUTION_FILE_OR_DIR;

		/**********************************
		 * FASE 1 - Verifica os Par�metros e Carrega as Configura��es Gerais da
		 * aplica��o
		 **********************************/

		log.info("***********************************");
		log.info("* Verificando par�metros iniciais..");
		log.info("***********************************");
	   
		
			// 1� Par�metro: Caminho do arquivo de execu��o (ou pasta onde est�
			// contido o arquivo)
			if (args[0] != null) {
				executionFileOrDir = args[0].replace("\\", "\\\\");
			}
			// Verifica demais par�metros, que podem ser:
			//
			if (args.length > 1) {
				for (int i = 1; i < args.length; i++) {
					String currArg = args[i];
					// Verifica se � para ser executado em ambiente de
					// desenvolvimento
					if (IStringConstants.DEV_ENVIRONMENT.equalsIgnoreCase(currArg)) {
						log.warn("Definida Execu��o em Ambiente de Desenvolvimento!");
						Environment.getInstance().setDevMode();
					} else
					// Verifica se � para ser executado Rollback da Vers�o
					if (ExecutionMode.ROLLBACK.toString().equalsIgnoreCase(currArg)) {
						log.warn("Definida Execu��o de ROLLBACK da Vers�o!");
						Environment.getInstance().setExecutionMode(
								ExecutionMode.ROLLBACK);
					}
				}
			//}
		}

		log.info("****************************************");
		log.info("* Carregando as configura��es iniciais..");
		log.info("****************************************");
		try {
			String config = System.getProperty("config.dir");
			String urlAux = IStringConstants.SQL_EXECUTOR_PROPERTIES_FILE;
			if (config != null) {
				urlAux = config + "/"
						+ IStringConstants.SQL_EXECUTOR_PROPERTIES_FILE;
			}
			Properties properties = new PropertyUtil().getProperties(urlAux);
			if (properties != null) {
				appName = properties.getProperty("AppName", "");
				companyName = properties.getProperty("CompanyName", "");
				companyGroup = properties.getProperty("CompanyGroup", "");
				stopOnError = Boolean.valueOf(properties.getProperty(
						"StopOnError", "TRUE"));
				connectionApplyPatchLog = properties.getProperty(
						"ConnecToRegisterApplyPatchLog", "");
			}
		} catch (IOException e) {
			log.error("Erro ao ler o arquivo de configura��o "
					+ IStringConstants.ENVIRONMENT_PROPERTIES_FILE + ": " + e);
		}

		/**********************************
		 * FASE 2 - Carrega os Scripts
		 **********************************/
		log.info("*************************");
		log.info("* Carregando os Scripts..");
		log.info("*************************");
		Boolean allScriptsLoaded = true;

		VersionParser versionParser = new VersionParser();
		ArrayList<ProductVersion> versionsList = versionParser
				.loadVersion(new File(executionFileOrDir), 0);
		
		

			if (versionsList == null || versionsList.isEmpty()) {
				log.error("N�o foi carregada nenhuma vers�o do caminho encontrado: "
						+ executionFileOrDir);
			} else {
				for (ProductVersion productVersion : versionsList) {
					if (productVersion.hasProblem()) {
						allScriptsLoaded = false;
						log.fatal("Encontrados problemas nos Scripts da Vers�o: "
								+ productVersion.getVersion());
						productVersion.printProblems(log);
					} else {
						log.info("N�o foi encontrado nenhum problema nos Scripts da Vers�o: "
								+ productVersion.getVersion());
					}
				}
			}
			
		
		// Caso n�o tenha conseguido carregar todos os scripts, n�o d�
		// andamento.
		if (allScriptsLoaded) {
			/***************************************
			 * FASE 3 - Conecta na Base de Dados
			 ***************************************/
			log.info("************************");
			log.info("* Carregando Conex�es..*");
			log.info("************************");
			DBConnectionManager dbConnectionManager = null;

			// Boolean dataBaseConnLoaded = false;
			if (allScriptsLoaded && loadDataBase) {

				DBConnectionParser connectionParser = new DBConnectionParser();
				dbConnectionManager = connectionParser.getDBConnectionManager();
				if (dbConnectionManager.hasConnectionError()) {
					log.warn("N�o foi poss�vel conectar em todos "
							+ "os schemas configurados " + "no arquivo "
							+ IStringConstants.ENVIRONMENT_PROPERTIES_FILE);
					dbConnectionManager.printDBConnectionError();
				} else {
					log.info("Conex�o com a Base de Dados bem sussedida.");
					// dataBaseConnLoaded = true;
				}
			}

			/***************************************
			 * FASE 4 - Valida as conex�es requeridas pelos scripts
			 ***************************************/
			log.info("**********************");
			log.info("* Validando Conex�es..");
			log.info("**********************");
			Boolean hasAllRequiredConnections = false;
			SQLExecutorManager sqlExecutorManager = new SQLExecutorManager(
					appName, companyName, companyGroup);

			if (dbConnectionManager != null) {
				sqlExecutorManager.stopOnError(stopOnError);
				sqlExecutorManager
						.setConnectionApplyPatchLog(dbConnectionManager
								.getConnection(connectionApplyPatchLog));

				String requiredConnection = "";

				// if (scriptsLoaded && dataBaseConnLoaded) {
				if (allScriptsLoaded) {
					requiredConnection = sqlExecutorManager
							.getDisconnectedRequiredDataSources(versionsList,
									dbConnectionManager);
					if (requiredConnection == null
							|| requiredConnection.length() == 0) {
						hasAllRequiredConnections = true;
						log.info("Cont�m todas as conex�es necess�rias");
					} else {
						log.fatal("N�o cont�m todas as conex�es necess�rias: "
								+ requiredConnection);
					}
				}
			}

			/***************************************
			 * FASE 5 - Valida scripts j� aplicados
			 ***************************************/
			// TODO: Implementar na vers�o 2.0

			/***************************************
			 * FASE 6 - Executa os scripts na base
			 ***************************************/
			if (executeScripts && hasAllRequiredConnections) {
				log.info("******************************************");
				log.info("* Executando os Scripts na base de dados..");
				log.info("******************************************");
				
				if(ExecutionMode.ROLLBACK.toString().equalsIgnoreCase(
						Environment.getInstance().getExecutionMode().toString())){
					
					
					//DBConnectionManager dbConnectionManager = null;
					ArrayList<String> pathList = sqlExecutorManager.getLastPatch(dbConnectionManager);
					VersionParser versionParserRoolback = new VersionParser();
					String version = versionParserRoolback.loadVersionRollback(new File(executionFileOrDir), pathList);
					
					ArrayList<ProductVersion> versionsList1 = versionParserRoolback.loadVersion(new File(version), 0);
					sqlExecutorManager.applyScripts(Environment.getInstance()
							.getExecutionMode(), versionsList1, dbConnectionManager, 0);//1 -> swing| 0 ->prompt
					//verificar scripts do banco de dados -> pegar uma lista dos scripts do banco
					//comparar com os scripts existentes em cada diretorio -- metodo que compara retornando true/false e o diretorio Hashmap<String,String>
								
				}else{
					sqlExecutorManager.applyScripts(Environment.getInstance()
							.getExecutionMode(), versionsList, dbConnectionManager, 0);
				}
				
				
			}
			log.info("********************************");
			log.info("* Finalizada aplica��o da Vers�o");
			log.info("********************************");
		}
	}
}
