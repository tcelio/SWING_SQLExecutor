package com.indra.executor;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.indra.db.DBConnection;
import com.indra.db.connection.DBConnectionManager;
import com.indra.executor.register.RegisterSQLExecution;
import com.indra.sql.ProductVersion;
import com.indra.sql.SQLType;
import com.indra.sql.ScriptData;
import com.indra.sql.ScriptFile;
import com.indra.util.ExecutionMode;

/**
 * Classe respons�vel por fazer a conex�o com o Banco de Dados e executar os
 * Scripts contidos nas Vers�es.
 * 
 * @author rmarini
 * 
 */
public class SQLExecutorManager {

	private static final Log log = LogFactory.getLog(SQLExecutorManager.class);

	private String appName = "";
	private String companyName = "";
	private String companyGroup = "";
	private DBConnection connectionApplyPatchLog = null;

	private boolean stopOnError = true;

	RegisterSQLExecution registerSQLExecution = new RegisterSQLExecution();

	public SQLExecutorManager(String appName, String companyName, String companyGroup) {
		registerSQLExecution.setSysUser(System.getProperty("user.name"));
		try {
			registerSQLExecution.setSysHostName(InetAddress.getLocalHost().getHostName());
		} catch (UnknownHostException e) {
			log.warn("N�o foi poss�vel obter o nome do Host", e);
		}
		this.appName = appName;
		this.companyName = companyName;
		this.companyGroup = companyGroup;
	}

	/**
	 * Define a Conex�o onde ser�o registrados os logs da aplica��o da vers�o.
	 * 
	 * @param dbConnection
	 *            Objeto DBConnection onde ser�o registrados os logs da
	 *            aplica��o da vers�o.
	 */
	public void setConnectionApplyPatchLog(DBConnection dbConnection) {
		if (dbConnection != null) {
			connectionApplyPatchLog = dbConnection;
		}
	}

	/**
	 * Define se a aplica��o deve ou n�o parar a execu��o dos scripts em caso de
	 * Erro.
	 * 
	 * @param stopOnError
	 *            Deve (<code>true</code>) ou n�o <code>false</code> parar a
	 *            execu��o dos scripts em caso de Erro.
	 */
	public void stopOnError(boolean stopOnError) {
		this.stopOnError = stopOnError;
	}

	public String getDisconnectedRequiredDataSources(ArrayList<ProductVersion> versionsList,
			DBConnectionManager dbConnectionManager) {
		StringBuilder retVal = new StringBuilder();
		for (ProductVersion productVersion : versionsList) {
			for (String requiredDataSource : productVersion.getRequiredDataSource()) {
				// Se n�o tiver o DataSource Configurado, j� retorna como erro
				if (!dbConnectionManager.hasDataSource(requiredDataSource)) {
					retVal.append(requiredDataSource);
				} else if (dbConnectionManager.getConnection(requiredDataSource).getConnection() == null) {
					/*
					 * Se houver o DataSource, mas n�o estiver conectado, tenta
					 * conectar mais uma vez, se n�o conseguir retorna como
					 * erro.
					 */
					if (dbConnectionManager.getConnection(requiredDataSource).getConnection() == null) {
						DBConnection connection = dbConnectionManager.retryMakeConnection(requiredDataSource);
						if (connection == null || !connection.isConnected()) {
							retVal.append(requiredDataSource);
						}
					}
				}
			}
		}
		return retVal.toString();
	}

	/**
	 * Aplica��o os scripts do patch na base de dados.
	 * 
	 * @param executionMode
	 *            Indica o modo de Execu��o dos scripts: PATCH ou ROLLBACK
	 * @param versionsList
	 *            Lista contendo os scripts das vers�es que ser�o utilizados
	 * @param dbConnectionManager
	 *            Lista contendo as conex�es que ser�o utilizadas na aplica��o
	 *            dos scripts
	 * @return <code>True</code> em caso de sucesso e <code>False</code> em caso
	 *         de erro na aplica��o do patch.
	 */
	public Boolean applyScripts(ExecutionMode executionMode, ArrayList<ProductVersion> versionsList,
			DBConnectionManager dbConnectionManager) {
		Boolean allExecutionSucess = Boolean.TRUE;

		// create table if not exists
		registerSQLExecution.verifyControlVersionTable(connectionApplyPatchLog);

		if (ExecutionMode.PATCH.equals(executionMode)) {
			allExecutionSucess = applyPatch(versionsList, dbConnectionManager);
		} else {
			allExecutionSucess = applyRollBack(versionsList, dbConnectionManager);
		}
		return allExecutionSucess;
	}

	private Boolean applyPatch(ArrayList<ProductVersion> versionsList, DBConnectionManager dbConnectionManager) {
		Boolean allPatchSucess = Boolean.TRUE;
		for (ProductVersion productVersion : versionsList) {
			if (canApplyVersionOnCompany(productVersion)) {
				log.info("Aplicando vers�o " + productVersion.getVersion());

				try {
					Boolean executionSucess = executeScripts(ExecutionMode.PATCH, productVersion, dbConnectionManager);
					if (executionSucess) {
						registerSQLExecution.updateVersionApp(connectionApplyPatchLog, productVersion);
					} else {
						allPatchSucess = Boolean.FALSE;
						if (stopOnError) {
							log.warn("Aplica��o de Vers�o Interrompida."
									+ " Ocorreram Erros durante a aplica��o dos Scripts.");
							break;
						} else {
							registerSQLExecution.updateVersionApp(connectionApplyPatchLog, productVersion);
						}
					}
				} catch (Exception e) {
					allPatchSucess = Boolean.FALSE;
					if (stopOnError) {
						log.warn("Aplica��o de Vers�o Interrompida."
								+ " Ocorreram Erros durante a aplica��o dos Scripts.");
						break;
					}
				}
				// Altera o numero da vers�o da Aplica��o
				if (allPatchSucess) {
					log.info("Alterar o numero da vers�o para: " + productVersion.getVersion());
				} else {
					log.error("Falha na execu��o dos scripts." + " Vers�o aplicada com erros");
				}
			} else {
				log.info("Ignorando vers�o " + productVersion.getVersion() + " para Grupo/Companhia: " + companyGroup
						+ "/" + companyName);
			}
		}
		return allPatchSucess;
	}

	private Boolean applyRollBack(ArrayList<ProductVersion> versionsList, DBConnectionManager dbConnectionManager) {
		Boolean allRollbackSucess = Boolean.TRUE;

		ArrayList<ProductVersion> rollbackVersionsList = getLastVersionPatchExecuted(versionsList);

		for (ProductVersion productVersionRollback : rollbackVersionsList) {
			//ponto importantissimo!! Filtro de execucao baseado em empresas e grupos!
			if (canApplyVersionOnCompany(productVersionRollback)) {
				log.info("Aplicando Rollback da vers�o " + productVersionRollback.getVersion());

				try {
					Boolean executionSucess = executeScripts(ExecutionMode.ROLLBACK, productVersionRollback,
							dbConnectionManager);
					if (executionSucess) {
						log.info("Scripts SQL de Rollback executado com sucesso! ");

						break;
					} else {

					}
				} catch (Exception e) {
					allRollbackSucess = Boolean.FALSE;
					if (stopOnError) {
						log.warn("Aplica��o de Rollback Interrompida."
								+ " Ocorreram Erros durante a aplica��o dos Scripts.");
						break;
					}
				}
			}
		}

		return allRollbackSucess;
	}

	private ArrayList<ProductVersion> getLastVersionPatchExecuted(ArrayList<ProductVersion> versionsList) {
		ArrayList<ProductVersion> retVal = new ArrayList<ProductVersion>();

		ArrayList<ProductVersion> lastVersionPatch = registerSQLExecution.getLastVersionPatch(appName,
				connectionApplyPatchLog);

		Collections.reverse(versionsList);

		Boolean rollbackFounded = false;
		ProductVersion previousVersionApplied = null;

		for (ProductVersion executedProductVersion : lastVersionPatch) {
			if (!rollbackFounded && previousVersionApplied == null) {

				// bug???
				if (ExecutionMode.ROLLBACK.equals(executedProductVersion.getExecutionMode())) {
					rollbackFounded = true;

					// ---------------parte inserida para correcao
					// ---------------deve retornar um objeto e nao retornava nada!
					ProductVersion versionFoundedInVersionList = null;
					for (ProductVersion rollbackProductVersion : versionsList) {
							if (executedProductVersion.getVersion().equals(rollbackProductVersion.getVersion())) {
								versionFoundedInVersionList = rollbackProductVersion;
								break;
							}
					}
					if (versionFoundedInVersionList != null) {
						versionsList.remove(versionFoundedInVersionList);
						retVal.add(versionFoundedInVersionList);
					} else if (previousVersionApplied == null) {
						previousVersionApplied = executedProductVersion;
						break;
					}
					// ----------------------------fim-----------------------------------
					
				} else if (ExecutionMode.PATCH.equals(executedProductVersion.getExecutionMode())) {
					ProductVersion versionFoundedInVersionList = null;
					for (ProductVersion rollbackProductVersion : versionsList) {
						if (executedProductVersion.getVersion().equals(rollbackProductVersion.getVersion())) {
							versionFoundedInVersionList = rollbackProductVersion;
							break;
						}
					}
					if (versionFoundedInVersionList != null) {
						versionsList.remove(versionFoundedInVersionList);
						retVal.add(versionFoundedInVersionList);
					} else if (previousVersionApplied == null) {
						previousVersionApplied = executedProductVersion;
						break;
					}
				}
			}
		}
		return retVal;
	}

	/**
	 * Verifica se a Vers�o pode ser aplicada na Companhia ou Grupo
	 * 
	 * @param productVersion
	 *            Vers�o a ser verificada
	 * @return Se pode ou n�o executar a Vers�o
	 */
	private boolean canApplyVersionOnCompany(ProductVersion productVersion) {
		if (!productVersion.hasCompanyDefinition()) {
			return true;
		} else {
			if (companyGroup.isEmpty() && companyName.isEmpty()) {
				return true;
			} else if (!companyGroup.isEmpty() && productVersion.canExecuteInCompany(companyGroup)) {
				return true;
			} else if (!companyName.isEmpty() && productVersion.canExecuteInCompany(companyName)) {
				return true;
			}
			return false;
		}
	}

	/**
	 * Verifica se o Script pode ser aplicada na Companhia ou Grupo
	 * 
	 * @param productVersion
	 *            Vers�o a ser verificada
	 * @return Se pode ou n�o executar a Vers�o
	 */
	private boolean canApplyScriptOnCompany(ScriptFile scriptFile) {
		if (!scriptFile.hasCompanyDefinition()) {
			return true;
		} else {
			if (companyGroup.isEmpty() && companyName.isEmpty()) {
				return true;
			} else if (!companyGroup.isEmpty() && scriptFile.canExecuteInCompany(companyGroup)) {
				return true;
			} else if (!companyName.isEmpty() && scriptFile.canExecuteInCompany(companyName)) {
				return true;
			}
			return false;
		}
	}

	/**
	 * Aplica os scripts associados ao Patch ou Rollback da vers�o.
	 * 
	 * @param productVersion
	 * @param dbConnectionManager
	 * @return
	 */
	private Boolean executeScripts(ExecutionMode executionMode, ProductVersion productVersion,
			DBConnectionManager dbConnectionManager) {
		Boolean allExecuted = Boolean.TRUE;
		if (productVersion != null) {

			registerSQLExecution.registerScriptExecution(executionMode, connectionApplyPatchLog, productVersion);

			Map<Integer, ScriptFile> scriptFiles = null;
			if (ExecutionMode.PATCH.equals(executionMode)) {
				scriptFiles = productVersion.getScriptPatchFiles();
			} else {
				scriptFiles = productVersion.getScriptRollbackFilesOrdened();
			}

			for (int index = 0; index < scriptFiles.size(); index++) {
				ScriptFile scriptFile = scriptFiles.get(index);
				if (scriptFile != null) {
					String execResult = "SUCESS";
					log.debug("Verificando Script: " + scriptFile.getFileName());
					if (canApplyScriptOnCompany(scriptFile)) {
						Boolean executedScriptFile = executeScriptFile(scriptFile, dbConnectionManager);
						if (executedScriptFile) {
							registerSQLExecution.registerScriptExecuted(connectionApplyPatchLog, productVersion,
									scriptFile, execResult, "");
						} else {
							allExecuted = Boolean.FALSE;
							execResult = "ERROR";
							registerSQLExecution.registerScriptExecuted(connectionApplyPatchLog, productVersion,
									scriptFile, execResult, scriptFile.getExecutionException().getLocalizedMessage());
							if (stopOnError) {
								break;
							}
						}
					} else {
						log.info("Ignorando Script " + scriptFile.getFileName() + " para Grupo/Companhia: "
								+ companyGroup + "/" + companyName);
					}
				}
			}
		}
		return allExecuted;
	}

	private Boolean executeScriptFile(ScriptFile scriptFile, DBConnectionManager dbConnectionManager) {
		Boolean allExecuted = Boolean.TRUE;

		if (scriptFile != null && scriptFile.getScriptDataPool() != null) {
			int afectedRows = 0;
			for (int index = 0; index < scriptFile.getScriptDataPool().size(); index++) {
				log.debug("Executando Bloco " + index);
				Boolean executedSucess = Boolean.TRUE;
				ScriptData scriptData = scriptFile.getScriptDataPool().get(index);

				replaceDataSourceReferences(scriptData, dbConnectionManager);

				DBConnection connection = dbConnectionManager.getConnection(scriptData.getDataSource());
				try {
					if (SQLType.DDL.equals(scriptData.getScriptType())) {
						executedSucess = executeDDL(scriptData, connection);
					} else if (SQLType.DML.equals(scriptData.getScriptType())) {
						int executeDML = executeDML(scriptData, connection);
						afectedRows = afectedRows + executeDML;
						log.debug("Registros Afetados: " + executeDML);
					} else {
						executedSucess = Boolean.FALSE;
					}
				} catch (SQLException e) {
					log.error("Erro ao executar script " + scriptFile.getFileName() + ": " + e);
					scriptFile.setExecutionException(e);
					allExecuted = Boolean.FALSE;
					break;
				}
				if (executedSucess) {
					log.debug("Bloco " + index + " aplicado com Sucesso");
				} else {
					allExecuted = Boolean.FALSE;
				}
			}
			log.debug("Registros Alterados: " + afectedRows);
			if (allExecuted) {
				log.info("Script " + scriptFile.getFileName() + " aplicado com sucesso");
			}
		}
		return allExecuted;
	}

	/**
	 * Substitui no Script a ser executado Refer�ncias de schema pelo nome
	 * correto do schema.
	 * 
	 * @param scriptData
	 * @param dbConnectionManager
	 */
	private void replaceDataSourceReferences(ScriptData scriptData, DBConnectionManager dbConnectionManager) {
		if (scriptData != null && dbConnectionManager != null) {
			ArrayList<String> referencedDataSourceOrdened = orderDataSourceNames(scriptData.getReferencedDataSource());
			for (String dataSourceName : referencedDataSourceOrdened) {
				DBConnection dbConnection = dbConnectionManager.getConnection(dataSourceName);
				if (dbConnection != null && dbConnection.getSchemaName() != null) {
					String replacedSQL = scriptData.getScript().toString().replace("&" + dataSourceName,
							dbConnection.getSchemaName());
					scriptData.setScript(replacedSQL);
				}
			}
		}
	}

	/**
	 * Ordena a lista de Nome de DataSources pelo maior nome para o menor.
	 * 
	 * @param referencedDataSource
	 * @return
	 */
	private static ArrayList<String> orderDataSourceNames(ArrayList<String> referencedDataSource) {
		String[] teste = new String[] {};
		teste = referencedDataSource.toArray(teste);
		for (int i = teste.length - 1; i >= 1; i--) {
			for (int j = 0; j < i; j++) {
				if (teste[j].length() < teste[j + 1].length()) {
					String aux = teste[j];
					teste[j] = teste[j + 1];
					teste[j + 1] = aux;
				}
			}
		}
		return new ArrayList<String>(Arrays.asList(teste));
	}

	private Boolean executeDDL(ScriptData scriptData, DBConnection connection) throws SQLException {
		Boolean retVal = Boolean.TRUE;
		log.debug("Executando DDL: " + scriptData.getScript());
		Statement createStatement = connection.getConnection().createStatement();
		createStatement.execute(scriptData.getScript().toString());
		// System.out.println("script
		// running:"+scriptData.getScript().toString());
		return retVal;
	}

	private int executeDML(ScriptData scriptData, DBConnection connection) throws SQLException {
		int executeUpdate = 0;
		// Se for up script apenas com commit, ignora. N�o precisa de executar
		if (String.valueOf("COMMIT").equals(scriptData.getScript().toString().toUpperCase().replace(";", ""))) {
			log.info("Ignorando linha com apenas COMMIT no script");
		} else {
			log.debug("Executando DML: " + scriptData.getScript());
			Statement createStatement = connection.getConnection().createStatement();
			executeUpdate = createStatement.executeUpdate(scriptData.getScript().toString());
		}
		return executeUpdate;
	}

}
