package com.indra.executor.register;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.indra.db.DBConnection;
import com.indra.sql.ProductVersion;
import com.indra.sql.ScriptFile;
import com.indra.util.Environment;
import com.indra.util.ExecutionMode;

/**
 * Classe respons�vel por registrar as vers�es aplicada na base de dados. Al�m
 * disso, ela valida se as tabelas de controle de vers�o est�o criadas, e as
 * cria caso necess�rio.
 * 
 */
public class RegisterSQLExecution {

	private static final Log log = LogFactory
			.getLog(RegisterSQLExecution.class);

	private static String TABLE_LIST = "ZEUS_VERSION_APP,ZEUS_VERSION_APPLIED,ZEUS_VERSION_APPLIED_SCRIPTS";
	private static String SEQUENCE_LIST = "SEQ_ZEUS_VERSION_APP,SEQ_ZEUS_VERSION_APPLIED,SEQ_ZEUS_VERSION_APP_SCRIPTS";
	private static String SQL_VERIFY_TABLE_NAME = "SELECT COUNT(1) AS TOTAL FROM USER_TABLES WHERE TABLE_NAME = '%TABLE_NAME%'";
	private static String SQL_VERIFY_SEQUENCE = "SELECT COUNT(1) AS TOTAL FROM USER_SEQUENCES WHERE SEQUENCE_NAME = '%SEQUENCE_NAME%'";

	private static Map<String, String> sqlCreateTableList = new HashMap<String, String>();
	private static Map<String, String> sqlCreateSequenceList = new HashMap<String, String>();

	private String sysUser;
	private String sysHost;

	// private static String SQL_VERIFY_COLUMNS =
	// "SELECT COUNT(1) FROM USER_TAB_COLUMNS C WHERE C.TABLE_NAME = '&TABLE_NAME'";

	/**
	 * Seta o usuario logado no sistema.
	 * 
	 * @param sysUser
	 *            Nome do usuario logado na maquina.
	 */
	public void setSysUser(String sysUser) {
		if (sysUser != null) {
			this.sysUser = sysUser;
		}
	}

	/**
	 * Seta o nome da M�quina que est� sendo utilizada.
	 * 
	 * @param hostName
	 *            Nome da Maquina.
	 */
	public void setSysHostName(String hostName) {
		if (hostName != null) {
			this.sysHost = hostName;
		}
	}

	/**
	 * Verifica se as tabelas de controle de vers�o est�o criadas. Caso n�o
	 * esteja, executa um script para realizar sua cria��o.
	 * 
	 * @return Se as tabelas est�o criadas e prontas para utiliza��o.
	 */
	public boolean verifyControlVersionTable(DBConnection dbConnection) {
		boolean retVal = false;
		if (dbConnection.isConnected()) {
			for (String tableName : getTableList()) {
				retVal = createTableIfNotExists(dbConnection, tableName);
				// Caso encontre problemas em uma das tabelas, j� retorna a
				// excess�o para n�o permitir que sigam com a execu��o, pois sem
				// as tabelas n�o ser� poss�vel registrar as vers�es.
				if (!retVal) {
					break;
				}
			}
			// Se as tabelas est�o ok, verifica as sequences.
			if (retVal) {
				for (String sequenceName : getSequenceList()) {
					retVal = createSequenceIfNotExists(dbConnection,
							sequenceName);
					// Caso encontre problemas em uma das sequences, j� retorna
					// a excess�o para n�o permitir que sigam com a execu��o,
					// pois sem as sequences n�o ser� poss�vel registrar as
					// vers�es.
					if (!retVal) {
						break;
					}
				}
			}
		}
		return retVal;
	}

	/**
	 * Registra o Patch ou Rollback aplicados e atualiza o objeto productVersion
	 * com o ID registrado na base.
	 * 
	 * @param executionMode
	 *            Modo de Execu��o do Script (PATCH ou ROLLBACK)
	 * @param dbConnection
	 *            Conex�o com o Banco.
	 * @param productVersion
	 *            Objeto com o Patch ou Rollback.
	 * @return Se a execu��o foi feita com sucesso <code>true</code> ou n�o
	 *         <code>false</code>.
	 */
	public boolean registerScriptExecution(ExecutionMode executionMode,
			DBConnection dbConnection, ProductVersion productVersion) {
		boolean retVal = false;
		if (dbConnection != null && productVersion != null
				&& productVersion.getVersion() != null
				&& !Environment.getInstance().isDevMode()) {
			Long registeredVersion = Long.valueOf(0);
			// Toda vez que uma vers�o for aplicada na base, deve ser
			// registrada, mesmo que a vers�o j� tenha sido aplicada
			// anteriormente.
			// Long registeredVersion = getRegisteredVersion(dbConnection,
			// "INGRID", productVersion.getVersion());
			// if (Long.valueOf(0).equals(registeredVersion)) {
			PreparedStatement pstmt = null;
			try {
				pstmt = dbConnection
						.getConnection()
						.prepareStatement(
								"insert into zeus_version_applied(app_id, app_name, app_version, app_version_type, app_applied_date, app_user, app_machine) "
										+ " values(seq_zeus_version_applied.nextval, ?, ?, ?, ?, ?, ?)");
				pstmt.setString(1, "INGRID");
				pstmt.setString(2, productVersion.getVersion().toUpperCase()
						.replace("INGRID", "").trim());
				pstmt.setString(3, executionMode.toString());
				pstmt.setTimestamp(4, new Timestamp(Calendar.getInstance()
						.getTimeInMillis()));
				pstmt.setString(5, sysUser);
				pstmt.setString(6, sysHost);
				pstmt.executeUpdate();
				pstmt.close();

				registeredVersion = getRegisteredVersion(dbConnection,
						"INGRID", productVersion.getVersion());

			} catch (SQLException e) {
				log.error("Erro ao registrar Vers�o "
						+ productVersion.getVersion() + ": " + e);
			} finally {
				try {
					if (pstmt != null) {
						pstmt.close();
					}
				} catch (SQLException e) {
				}
			}
			// }
			productVersion.setVersionAppliedID(registeredVersion);
		}
		return retVal;
	}

	/**
	 * Obt�m o ID da vers�o registrada de acordo com a Aplica��o e sua Vers�o.
	 * 
	 * @param dbConnection
	 *            Conex�o com o Banco de Dados.
	 * @param appName
	 *            Nome da Aplica��o.
	 * @param version
	 *            Vers�o da Aplica��o.
	 * @return ID registrado da vers�o da aplica��o
	 *         <code>(retorna zero caso n�o tenha sido registrada)</code>.
	 */
	private Long getRegisteredVersion(DBConnection dbConnection,
			String appName, String version) {
		Long retVal = Long.valueOf(0);
		PreparedStatement pstmt = null;
		try {
			pstmt = dbConnection
					.getConnection()
					.prepareStatement(
							"select max(app_id) as app_id from zeus_version_applied where app_name = ? and app_version = ?");
			pstmt.setString(1, appName);
			pstmt.setString(2, version.toUpperCase().replace("INGRID", "")
					.trim());
			ResultSet result = pstmt.executeQuery();
			while (result.next()) {
				retVal = result.getLong("app_id");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
			} catch (SQLException e) {
			}
		}
		return retVal;
	}

	public ArrayList<ProductVersion> getLastVersionPatch(String appName,
			DBConnection dbConnection) {
		ArrayList<ProductVersion> retVal = new ArrayList<ProductVersion>();
		PreparedStatement pstmt = null;
		try {
			pstmt = dbConnection
					.getConnection()
					.prepareStatement(
							"select a.app_id, a.app_version, a.app_version_type from zeus_version_applied a where app_name = ? order by a.app_id desc");
			pstmt.setString(1, appName.toUpperCase());
			ResultSet result = pstmt.executeQuery();

			while (result.next()) {
				ProductVersion productVersion = new ProductVersion();
				productVersion.setVersionAppliedID(result.getLong("app_id"));
				productVersion.setVersion(result.getString("app_version"));
				String versionTypeString = result.getString("app_version_type");
				if (ExecutionMode.PATCH.toString().equalsIgnoreCase(
						versionTypeString)) {
					productVersion.setExecutionMode(ExecutionMode.PATCH);
				} else if (ExecutionMode.ROLLBACK.toString().equalsIgnoreCase(
						versionTypeString)) {
					productVersion.setExecutionMode(ExecutionMode.ROLLBACK);
				}
				retVal.add(productVersion);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
			} catch (SQLException e) {
			}
		}
		return retVal;
	}

	/**
	 * Registra na base a execu��o dos scripts da vers�o.
	 * 
	 * @param dbConnection
	 *            Conex�o onde ser� gravado o log de aplica��o.
	 * @param productVersion
	 *            Objeto com a Vers�o do Produto.
	 * @param script
	 *            Objeto com o Script executado.
	 * @param execReturn
	 *            Retorno da execu��o do script na base.
	 * @param execNotes
	 *            Observa��es da execu��o do script na base.
	 */
	public void registerScriptExecuted(DBConnection dbConnection,
			ProductVersion productVersion, ScriptFile scriptFile,
			String execReturn, String execNotes) {
		if (dbConnection != null && productVersion != null
				&& scriptFile != null && execReturn != null
				&& !Environment.getInstance().isDevMode()) {
			PreparedStatement pstmt = null;
			try {
				pstmt = dbConnection
						.getConnection()
						.prepareStatement(
								"insert into zeus_version_applied_scripts(vas_id, vas_ver_app_id, vas_script_name, vas_owner_executed, vas_exec_return, vas_exec_notes) "
										+ " values(seq_zeus_version_app_scripts.nextval, ?, ?, ?, ?, ?)");
				pstmt.setLong(1, productVersion.getVersionAppliedID());
				pstmt.setString(2, scriptFile.getFileNameFromExecute());
				pstmt.setString(3, scriptFile.getScriptDataPool().get(0)
						.getDataSource());
				pstmt.setString(4, execReturn);
				if (execNotes != null && execNotes.length() > 2000) {
					pstmt.setString(5, execNotes.substring(0, 2000));
				} else {
					pstmt.setString(5, execNotes);
				}
				pstmt.executeUpdate();
			} catch (SQLException e) {
				log.error("Erro ao registrar Script "
						+ productVersion.getVersion() + ": " + e);
			} finally {
				try {
					if (pstmt != null) {
						pstmt.close();
					}
				} catch (SQLException e) {
				}
			}
		}
	}

	/**
	 * Atualiza o numero corrente da vers�o.
	 * 
	 * @param dbConnection
	 *            Conex�o de onde est� a tabela de controle de vers�o.
	 * @param productVersion
	 *            Objeto contendo a vers�o que foi aplicada.
	 * @return Se conseguiu <code>true</code> ou n�o <code>false</code>
	 *         atualizar a vers�o corrente atual.
	 */
	public boolean updateVersionApp(DBConnection dbConnection,
			ProductVersion productVersion) {
		boolean retVal = false;
		if (dbConnection != null && productVersion != null
				&& !Environment.getInstance().isDevMode()) {
			PreparedStatement pstmt = null;
			try {
				pstmt = dbConnection
						.getConnection()
						.prepareStatement(
								"update zeus_version_app v set v.app_version = ?, v.app_allows_prev = 0 where v.app_name = ?");
				pstmt.setString(1, productVersion.getVersion().toUpperCase()
						.replace("INGRID", "").trim());
				pstmt.setString(2, "INGRID");
				int affectedRows = pstmt.executeUpdate();
				pstmt.close();
				if (affectedRows == 0) {
					pstmt = dbConnection
							.getConnection()
							.prepareStatement(
									"insert into zeus_version_app(app_id, app_name, app_version, app_allows_prev) "
											+ "values(seq_zeus_version_app.nextval, ?, ?, 0)");
					pstmt.setString(1, "INGRID");
					pstmt.setString(2, productVersion.getVersion()
							.toUpperCase().replace("INGRID", "").trim());
					pstmt.executeUpdate();
					pstmt.close();
				}
				retVal = true;
			} catch (SQLException e) {
				log.error("Erro ao registrar Vers�o "
						+ productVersion.getVersion() + ": " + e);
			} finally {
				try {
					if (pstmt != null) {
						pstmt.close();
					}
				} catch (SQLException e) {
				}
			}
		}
		return retVal;
	}

	/**
	 * Obt�m um ArrayList com as tabelas envolvidas no controle de aplica��o de
	 * vers�o.
	 * 
	 * @return Lista com as tabelas envolvidas no controle de aplica��o de
	 *         vers�o.
	 */
	private ArrayList<String> getTableList() {
		ArrayList<String> retVal = new ArrayList<String>();
		for (String tableName : TABLE_LIST.split(",")) {
			retVal.add(tableName);
		}
		return retVal;
	}

	/**
	 * Obt�m um ArrayList com as sequences envolvidas no controle de aplica��o
	 * de vers�o.
	 * 
	 * @return Lista com as sequences envolvidas no controle de aplica��o de
	 *         vers�o.
	 */
	private ArrayList<String> getSequenceList() {
		ArrayList<String> retVal = new ArrayList<String>();
		for (String sequenceName : SEQUENCE_LIST.split(",")) {
			retVal.add(sequenceName);
		}
		return retVal;
	}

	/**
	 * Verifica se a tabela existe, e executa a sua cria��o caso n�o exista.
	 * 
	 * @param dbConnection
	 *            Conex�o que ser� utilizada para verificar ou criar as tabelas.
	 * @param tableName
	 *            Nome da tabela a ser verificada.
	 * @return <code>true/false<code> Se a tabela existe ou n�o (retornar� falso caso n�o tenha sido
	 *         poss�vel executar a cria��o da tabela).
	 */
	private boolean createTableIfNotExists(DBConnection dbConnection,
			String tableName) {
		boolean tableExists = false;
		if (dbConnection != null && tableName != null) {
			log.debug("Executando DDL: " + SQL_VERIFY_TABLE_NAME);
			try {
				Statement statement = dbConnection.getConnection()
						.createStatement();
				ResultSet result = statement.executeQuery(SQL_VERIFY_TABLE_NAME
						.replace("%TABLE_NAME%", tableName));
				int total = 0;
				while (result.next()) {
					total = result.getInt("TOTAL");
				}
				// Cria a tabela caso n�o seja encontrada
				if (total == 0) {
					log.info("Tabela de controle "
							+ tableName
							+ " n�o existe. Executando script para sua cria��o...");
					statement.execute(getSQLCreateTable(tableName));
				}
				tableExists = true;
			} catch (SQLException e) {
				log.error("Erro na valida��o/cria��o da tabela " + tableName
						+ ":" + e);
			}
		}
		return tableExists;
	}

	/**
	 * Verifica se a sequence existe, e executa a sua cria��o caso n�o exista.
	 * 
	 * @param dbConnection
	 *            Conex�o que ser� utilizada para verificar ou criar as tabelas.
	 * @param sequenceName
	 *            Nome da tabela a ser verificada.
	 * @return <code>true/false<code> Se a tabela existe ou n�o (retornar� falso caso n�o tenha sido
	 *         poss�vel executar a cria��o da tabela).
	 */
	private boolean createSequenceIfNotExists(DBConnection dbConnection,
			String sequenceName) {
		boolean sequenceExists = false;
		if (dbConnection != null && sequenceName != null) {
			log.debug("Executando DDL: " + SQL_VERIFY_SEQUENCE);
			try {
				Statement statement = dbConnection.getConnection()
						.createStatement();
				ResultSet result = statement.executeQuery(SQL_VERIFY_SEQUENCE
						.replace("%SEQUENCE_NAME%", sequenceName));
				int total = 0;
				while (result.next()) {
					total = result.getInt("TOTAL");
				}
				// Cria a tabela caso n�o seja encontrada
				if (total == 0) {
					log.info("Sequence "
							+ sequenceName
							+ " n�o existe. Executando script para sua cria��o...");
					statement.execute(getSQLCreateSequence(sequenceName));
				}
				sequenceExists = true;
			} catch (SQLException e) {
				log.error("Erro na valida��o/cria��o da sequence "
						+ sequenceName + ":" + e);
			}
		}
		return sequenceExists;
	}

	/**
	 * Obt�m o script para a cria��o da tabela.
	 * 
	 * @param tableName
	 *            Nome da tabela.
	 * @return String com o script de cria��o da tabela.
	 */
	private String getSQLCreateTable(String tableName) {
		if (sqlCreateTableList.isEmpty()) {
			loadSQLCreateTableList();
		}
		return sqlCreateTableList.get(tableName);
	}

	/**
	 * Obt�m o script para a cria��o das sequences.
	 * 
	 * @param sequenceName
	 *            Nome da Sequence.
	 * @return String com o script de cria��o da sequence.
	 */
	private String getSQLCreateSequence(String sequenceName) {
		if (sqlCreateSequenceList.isEmpty()) {
			loadSQLCreateSequenceList();
		}
		return sqlCreateSequenceList.get(sequenceName);
	}

	/**
	 * Carrega o Map com os scripts de cria��o das tabelas.
	 */
	private void loadSQLCreateTableList() {
		// Adiciona os scripts de cria��o das tabelas
		// ZEUS_VERSION_APP
		sqlCreateTableList
				.put("ZEUS_VERSION_APP",
						"begin "
								+ "execute immediate 'create table ZEUS_VERSION_APP(app_id NUMBER(15) not null, app_name VARCHAR2(50), app_version VARCHAR2(50), app_allows_prev NUMBER(1))';"
								+ "execute immediate 'alter table ZEUS_VERSION_APP add constraint VERSION_APP_PK primary key (APP_ID)';"
								+ "end;");
		// ZEUS_VERSION_APPLIED
		sqlCreateTableList
				.put("ZEUS_VERSION_APPLIED",
						"begin "
								+ "execute immediate 'create table ZEUS_VERSION_APPLIED(app_id NUMBER(15) not null, app_name VARCHAR2(50) not null, app_version VARCHAR2(50) not null, app_applied_date DATE not null, app_user VARCHAR2(50), app_machine VARCHAR2(50))';"
								+ "execute immediate 'alter table ZEUS_VERSION_APPLIED add constraint ZEUS_VERSION_APPLIED_PK primary key (APP_ID)';"
								+ "end;");
		// ZEUS_VERSION_APPLIED_SCRIPTS
		sqlCreateTableList
				.put("ZEUS_VERSION_APPLIED_SCRIPTS",
						"begin "
								+ "execute immediate 'create table ZEUS_VERSION_APPLIED_SCRIPTS( VAS_ID NUMBER(15) not null, VAS_VER_APP_ID NUMBER(15) not null, VAS_SCRIPT_NAME VARCHAR2(100) not null, VAS_OWNER_EXECUTED VARCHAR2(10) not null, VAS_EXEC_RETURN VARCHAR2(10) not null, VAS_EXEC_NOTES VARCHAR2(2000))';"
								+ "execute immediate 'alter table ZEUS_VERSION_APPLIED_SCRIPTS add constraint ZEUS_VER_APP_SCRIPTS_PK primary key (VAS_ID)';"
								+ "execute immediate 'alter table ZEUS_VERSION_APPLIED_SCRIPTS add constraint ZEUS_VER_APP_SCRIPTS_UK unique (VAS_VER_APP_ID, VAS_SCRIPT_NAME)';"
								+ "execute immediate 'alter table ZEUS_VERSION_APPLIED_SCRIPTS add constraint ZEUS_VER_APP_SCRIPTS_FK foreign key (VAS_VER_APP_ID) references ZEUS_VERSION_APPLIED (APP_ID)';"
								+ "end;");
	}

	/**
	 * Carrega o Map com os scripts de cria��o das sequences.
	 */
	private void loadSQLCreateSequenceList() {
		// Adiciona os scripts de cria��o das sequences
		// SEQ_ZEUS_VERSION_APP
		sqlCreateSequenceList.put("SEQ_ZEUS_VERSION_APP",
				"create sequence SEQ_ZEUS_VERSION_APP " + "minvalue 1 "
						+ "maxvalue 999999999999999999999999999 "
						+ "start with 41 " + "increment by 1 " + "cache 20");
		// SEQ_ZEUS_VERSION_APPLIED
		sqlCreateSequenceList.put("SEQ_ZEUS_VERSION_APPLIED",
				"create sequence SEQ_ZEUS_VERSION_APPLIED " + "minvalue 1 "
						+ "maxvalue 999999999999999999999999999 "
						+ "start with 1 " + "increment by 1 " + "cache 20");
		// SEQ_ZEUS_VERSION_APP_SCRIPTS
		sqlCreateSequenceList.put("SEQ_ZEUS_VERSION_APP_SCRIPTS",
				"create sequence SEQ_ZEUS_VERSION_APP_SCRIPTS " + "minvalue 1 "
						+ "maxvalue 999999999999999999999999999 "
						+ "start with 1 " + "increment by 1 " + "cache 20");
	}
}
